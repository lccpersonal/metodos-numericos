
import meshio
import matplotlib.pyplot as plt
import matplotlib.collections
import numpy as np
from scipy.sparse.linalg import spsolve
from scipy import sparse, linalg
import math

'''
==============================================
FEM2DHT
==============================================
'''


class fem2dht:
    def __init__(self):
        self.nnodes = 0
        self.nelements = 0

    def create_nodes(self, coords):
        self.nodes = coords
        self.nnodes = len(self.nodes)

    def create_elements(self, connectivities):
        self.elements = []
        self.connectivities = connectivities

        for item in connectivities:
            if len(connectivities) == 3:
                element = HT3(item)
            elif len(connectivities) == 4:
                element = HQ4(item)
            else:
                print('Not Valid')

            self.elements.append(element)
        self.nelements = len(self.elements)

    def define_props(self, props):
        for i, element in enumerate(self.elements):
            element.props = props

    def create_bc(self, bc_type, nodes, values):
        self.bcs_nodes = nodes
        self.bcs_values = values

    def solve(self):

        rows = []
        cols = []
        values = []

        for element in self.elements:
            r, c, v = element.Kmatrix(self.nodes)
            rows.append(r)
            cols.append(c)
            values.append(v)

        rows = np.array(rows, dtype='int').flatten()
        cols = np.array(cols, dtype='int').flatten()
        values = np.array(values, dtype='float').flatten()

        Kglobal = sparse.csr_matrix((values, (rows, cols)), shape=((self.nnodes, self.nnodes)))
        Kglobal = Kglobal + Kglobal.T - sparse.diags(Kglobal.diagonal(), dtype='float')

        self.Kglobal = Kglobal

        # TODO: Terminar Resolução do Sistema
        # 1) Montagem do vetor f
        w = 10e20
        fglobal = np.zeros(self.nnodes)

        # 2) Aplicações das BC no vetor fglobal
        # 3) Aplicar as condições de contorno na matroz Kglobal
        nbcs = len(self.bcs_nodes)
        for k in range(nbcs):
            node = self.bcs_nodes[k]
            fglobal[node] = self.bcs_values[k] * w
            Kglobal[node, node]  += w

        # 4) Resolver o problema
        self.T = spsolve(Kglobal, fglobal)

    def plot(self):

        plt.figure()
        plt.tripcolor(self.nodes[:, 0], self.nodes[:, 1], self.connectivities, self.T, shading='gouraud')
        plt.colorbar()
        plt.show()


class HT3:
    def __init__(self, nodes):
        self.nodes = nodes
        self.props = 0.0

    def Kmatrix(self, coords):
        x = coords[self.nodes, 0]
        y = coords[self.nodes, 1]

        B = np.zeros((2, 3))
        B[0][0] = y[1] - y[2]
        B[0][1] = y[2] - y[0]
        B[0][2] = y[0] - y[1]

        B[1][0] = x[2] - x[1]
        B[1][1] = x[0] - x[2]
        B[1][2] = x[1] - x[0]

        A = 0.5 * (x[0] * y[1] + y[0] * x[2] + x[1] * y[2] - x[2] * y[1] - x[0] * y[2] - x[1] * y[0])

        B = (1.0 / (2 * A)) * B

        K = self.props * np.matmul(B.transpose(), B) * A

        self.area = A
        self.centroid = [np.mean(x), np.mean(y)]

        ind_rows = [self.nodes[0], self.nodes[0], self.nodes[0], self.nodes[1], self.nodes[1], self.nodes[2]]
        ind_cols = [self.nodes[0], self.nodes[1], self.nodes[2], self.nodes[1], self.nodes[2], self.nodes[2]]
        values = [K[0, 0], K[0, 1], K[0, 2], K[1, 1], K[1, 2], K[2, 2]]

        return ind_rows, ind_cols, values


# TODO: Implementar o elemento quadrático
class HQ4:
    def __init__(self, nodes):
        self.nodes = nodes
        self.props = 0.0

    def Kmatriz(self, coords):

        # Propriedades Gauss -> Fisher
        csi = [-1/math.sqrt(3), -1/math.sqrt(3), 1/math.sqrt(3), 1/math.sqrt(3)]
        zeta = [1/math.sqrt(3), -1/math.sqrt(3), -1/math.sqrt(3), 1/math.sqrt(3)]
        phi = []

        for csi_, zeta_ in zip(csi, zeta):
            phi.append(0.5*(1 + csi_)*0.5*(1 + zeta_))

        x = coords[0, 0]*phi[0] + coords[0, 1]*phi[1] + coords[0, 2]*phi[2] + coords[0, 3]*phi[3]
        y = coords[1, 0]*phi[0] + coords[1, 1]*phi[1] + coords[1, 2]*phi[2] + coords[1, 3]*phi[3]

        dphi_dcsi = [0.25*(1+zeta), (-0.25)*(1+zeta), (-0.25)*(1-zeta), 0.25*(1-zeta)]
        dphi_dzeta = [0.25*(1+csi), (-0.25)*(1-csi), (-0.25)*(1-csi), 0.25*(1+csi)]

        dx_dcsi  = 0.25*(1 + zeta)*x[0] + (-0.25)*(1 + zeta)*x[1] + (-0.25)*(1 - zeta)*x[2] + 0.25*(1 - zeta)*x[3]
        dx_dzeta = 0.25*(1 + csi )*x[0] + (-0.25)*(1 - csi )*x[1] + (-0.25)*(1 - csi )*x[2] + 0.25*(1 + csi )*x[3]

        dy_dcsi  = 0.25*(1 + zeta)*y[0] + (-0.25)*(1 + zeta)*y[1] + (-0.25)*(1 - zeta)*y[2] + 0.25*(1 - zeta)*y[3]
        dy_dzeta = 0.25*(1 + csi )*y[0] + (-0.25)*(1 - csi )*y[1] + (-0.25)*(1 - csi )*y[2] + 0.25*(1 + csi )*y[3]

        J = np.asarray([[dx_dcsi, dx_dzeta], [dy_dcsi, dy_dzeta]])
        det_J = np.linalg.det(J)
        dphi_dnaocartesiano = np.concatenate((dphi_dcsi, dphi_dzeta), axis=0)
        B = np.matmul(J, dphi_dnaocartesiano)

        K = self.props*det_J*B.T*B
        print(K)
        pass
        #return ind_rows, ind_cols, values

'''
===============================================
MAIN
===============================================
'''
if __name__ == '__main__':

    problem = fem2dht()

    # TODO: alterar para a malha completa  - EX 1
    coords = [[0.0, 0.0], [2.0, 0.5], [0.0, 1.0], [2.0, 1.0]]
    coords = np.array(coords)
    #mesh = meshio.read('ex1_mesh1_tri.msh')
    #coords = np.array(mesh.points[:, 0:2])
    #connectivities = mesh.cells['triangle']

    # Elemento Tri
    #connectivities = ([0, 1, 2], [3, 2, 1])
    # Elemento Q4
    connectivities = ([0, 1, 2, 3])

    # (1) - Geometry and mesh
    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    # TODO: alterar para os nós da malha importada - Ex 1
    #nodes = [0, 3]
    #values = [2, 5]
    nodes = [0, 2, 4]
    values = [10, 5, 7.5]

    # (2) - Properties
    problem.define_props(5)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()












