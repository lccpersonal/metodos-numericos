
import numpy as np
import math

if __name__ == '__main__':

    F = lambda x, y: x**2 + 2*x + y**2 - 5

    csi = 1/math.sqrt(3)
    zeta = -1/math.sqrt(3)

    csi1 = 0.5*(1 + csi)*0.5*(1 + zeta)
    csi2 = 0.5*(1 - csi)*0.5*(1 + zeta)
    csi3 = 0.5*(1 - csi)*0.5*(1 - zeta)
    csi4 = 0.5*(1 + csi)*0.5*(1 - zeta)

    x_ = np.array([6, 3, 0, 4])
    y_ = np.array([1, 4, 0, -2])

    x = x_[0]*csi1 + x_[1]*csi2 + x_[2]*csi3 + x_[3]*csi4
    y = y_[0]*csi1 + y_[1]*csi2 + y_[2]*csi3 + y_[3]*csi4

    print('x:',  x,'y:', y, 'F:', F(x,y))


    dx_dcsi = (1/4)*x_[0] +  (-1/4)*x_[1] + (-1/4)*x_[2] + (1/4)*x_[3]
    dx_dzeta = (1/4)*x_[0] +  (1/4)*x_[1] + (-1/4)*x_[2] + (-1/4)*x_[3]

    dy_dcsi = (1/4)*y_[0] +  (-1/4)*y_[1] + (-1/4)*y_[2] + (1/4)*y_[3]
    dy_dzeta = (1/4)*y_[0] +  (1/4)*y_[1] + (-1/4)*y_[2] + (-1/4)*y_[3]

    print(dx_dcsi, dx_dzeta, dy_dcsi, dy_dzeta)

    J = np.array([[dx_dcsi, dx_dzeta], [dy_dcsi, dy_dzeta]])

    print(np.linalg.det(J))