clear all
close all
clc


tic 

%% Imput data
%coords = [0.0, 0.0; 4.0, 3.0; 6.0, -2.0];   %[coord_x coord_y]
%elements = [1 2; 2 3; 1 3];                 %[node_i node_j]
load('fem_data.mat');
%materials = [100e9 0.3];                    %[Youg nu]
%elem_props = [1 1; 1 1; 1 1];               %[materials_id section_id]
%section = [0.1];                            %[area]
%forces = [2 10e3 -50e3];                    %[node fx fy]
%disp = [1 1 0.0; 1 2 0.0; 3 1 0.0];         %[node dof value]

%% Processing
nnodes = size(coords, 1);
nelements = size(elements, 1);

ndofs = 2;

aux_r=zeros(nelements*10,1);
aux_c=zeros(nelements*10,1);
aux_v=zeros(nelements*10,1);

%K = zeros(nnodes*ndofs, nnodes*ndofs);
%f = zeros(nnodes*ndofs,1);


for k=1:nelements
    
    i = elements(k,1);   
    j = elements(k,2);
    
    h = sqrt(sum((coords(j,:) - coords(i,:)).^2));
    c = (coords(j,1) - coords(i,1))/h;
    s = (coords(j,2) - coords(i,2))/h;
    
    %E = materials(elem_props(k,1),1);
    %A = section(elem_props(k,2),1);
    E = 1.0;
    A = 1.0;
    
    v1 = (E*A/h)*c*c;
    v2 = (E*A/h)*c*s;
    v3 = (E*A/h)*s*s;

    rows=[2*i-1, 2*i-1, 2*i, 2*i-1, 2*i-1, 2*i,   2*i,   2*j-1, 2*j-1, 2*j];
    cols=[2*i-1, 2*i,   2*i, 2*j-1, 2*j,   2*j-1, 2*j,   2*j-1, 2*j  , 2*j];
    v =  [   v1    v2    v3    -v1   -v2     -v2   -v3      v1     v2  v3];
    
    aux_r(10*(k-1)+1: 10*k)=rows;
    aux_c(10*(k-1)+1: 10*k)=cols;
    aux_v(10*(k-1)+1: 10*k)=v;
%     
%     Ke =      [v1 v2 -v1 -v2;
%                v2 v3 -v2 -v3;
%                -v1 -v2 v1 v2;
%                -v2 -v3 v2 v3];   %Matriz Elementar
%            
%            
%     K(2*i-1, 2*i-1) = K(2*i-1, 2*i-1) +v1;
%     K(2*i-1, 2*i  ) = K(2*i-1, 2*i  ) +v2;
%     K(2*i  , 2*i  ) = K(2*i  , 2*i  ) +v3;
%     
%     K(2*i-1, 2*j-1) = K(2*i-1, 2*j-1) -v1;
%     K(2*i-1, 2*j  ) = K(2*i-1, 2*j  ) -v2;
%     K(2*i  , 2*j-1) = K(2*i  , 2*j-1) -v2;
%     K(2*i  , 2*j  ) = K(2*i  , 2*j  ) -v3;
% 
%     
%     K(2*j-1, 2*j-1) = K(2*j-1, 2*j-1) +v1;
%     K(2*j-1, 2*j  ) = K(2*j-1, 2*j  ) +v2;
%     K(2*j  , 2*j  ) = K(2*j  , 2*j  ) +v3;
%    
%     
end
K=sparse(aux_r, aux_c, aux_v, nnodes*ndofs, nnodes*ndofs);
K = K + K' -diag(diag(K));


%% Assembly of the global force vector

% for k = 1:size(forces,1)
%     
%     i = forces(k,1);
%     fx = forces(k,2);
%     fy = forces(k,3);
%     f(2*i-1) = f(2*i-1) + fx;
%     f(2*i) = f(2*i) + fy;
%     
% end


%% Boundary conditions (essecial BCs)

% Knew = K;       %Para n�o alterar a matriz K 
% fnew = f;
% w = 1e16;       %Associado ao n�mero de condicionamento da matriz
% 
% for k=1:size(disp,1)
%     
%     i = disp(k,1);
%     dof = disp(k,2);
%     value = disp(k,3);
%     
%     Knew(2*i-2+dof, 2*i-2+dof) = Knew(2*i-2+dof, 2*i-2+dof) + w;
%     fnew(2*i-2+dof) = value*w;
%     
% end
% 
% u=Knew\fnew

disp(['Time elapsed: ' num2str(toc) ' s'])

tic
u_aux = K(4:end, 4:end) \ ones(nnodes*ndofs-3,1);
toc

