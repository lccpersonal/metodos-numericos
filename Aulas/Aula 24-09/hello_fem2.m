%
% HELLO FEM
%

clear all
close all
clc

% [coord_x coord_y]

load('fem_data.mat')



%Processing
nnodes = size(coords,1);
nelements=size(elements,1);
ndofs=2;

K=zeros(nnodes*ndofs, nnodes*ndofs);


tic;

for k=1:nelements
   i = elements(k,1); 
   j=elements(k,2);
   h = sqrt(sum( (coords(j,:) - coords(i,:)).^2 ));
   c=(coords(j,1) - coords(i,1))/h;
   s=(coords(j,2) - coords(i,2))/h;
   
   E = 1.0;
   A=1.0;
   
   v1=c*c;
   v2=c*s;
   v3=s*s;
   
   Ke=(E*A/h) * [v1 v2 -v1 -v2; 
               v2 v3 -v2 -v3;
               -v1 -v2 v1 v2; 
               -v2 -v3 v2 v3];
   
   K(2*i-1:2*i, 2*i-1:2*i)= K(2*i-1:2*i,2*i-1:2*i) + Ke(1:2, 1:2);
   K(2*j-1:2*j, 2*j-1:2*j)= K(2*j-1:2*j,2*j-1:2*j)+ Ke(3:4, 3:4);
   
   K(2*j-1:2*j, 2*i-1:2*i)= K(2*j-1:2*j,2*i-1:2*i) + Ke(3:4, 1:2);
   K(2*i-1:2*i, 2*j-1:2*j)= K(2*i-1:2*i,2*j-1:2*j) + Ke(1:2, 3:4);
   
end

disp(['Time elapsed:', num2str(toc) 's'])
%
% %Assembly of the global force vector
% 
% for k=1:size(forces,1)
%     i=forces(k,1);
%     fx=forces(k,2); 
%     fy=forces(k,3);
%     
%     f(2*i-1) = f(2*i-1) +fx;
%     f(2*i) = f(2*i) + fy;
% end
% 
% 
% %Boundary conditions (essential BCs) -> Penalization
% Knew=K; fnew=f;
% w = 1e16;
% 
% for k=1:size(disp,1)
%    i = disp(k,1);
%    dof=disp(k,2);
%    value=disp(k,3);
%    
%    Knew(2*i - 2+ dof,2*i - 2+ dof) = K(2*i - 2+ dof,2*i - 2+ dof) + w ;
%    fnew(2*i - 2+ dof) = value*w; 
% end   
% 
% %Solve
% u = Knew \ fnew

