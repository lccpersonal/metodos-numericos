clear all
close all
clc

x = linspace(0,5,200);
y = linspace(0,1,50);

[xg, yg]= meshgrid(x,y);
coords=[xg(:), yg(:)];


tri=delaunay(xg(:), yg(:));
% 
% triplot(tri, xg(:), yg(:));
% axis equal
% axis off

elements= [];

for k=1:size(tri,1)
    elements(end+1,:) = sort([tri(k,1) tri(k,2)]);
    elements(end+1,:) = sort([tri(k,2) tri(k,3)]);
    elements(end+1,:) = sort([tri(k,3) tri(k,1)]);
end

elements=unique(elements, 'rows');

% figure;
% for k=1:size(elements,1)
%     x1 = coords(elements(k,1), 1);
%     y1= coords(elements(k,1), 2);
%     
%     x2 = coords(elements(k,2), 1);
%     y2= coords(elements(k,2), 2);
% 
%     plot([x1, x2], [y1, y2], '.-b');
%     hold on;
% end

% axis equal 
% axis off


save('fem_data', 'coords', 'elements')
