
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy import optimize

def model_exemplo1(p, x):
    mu = [p[0], p[1]]
    sig = [p[2], p[3]]
    return p[4]*np.exp(-np.power(x - mu[0], 2.) / (2 * np.power(sig[0], 2.))) + \
    p[5]*np.exp(-np.power(x - mu[1], 2.) / (2 * np.power(sig[1], 2.)))
    #result = optimize.least_squares(model_residue, [0.3, 0.5, 0.4, 0.4, 0.4, 0.4], jac='2-point', method='lm', args=(x,y))


def model_exemplo2(p, x):
    return np.exp(-p[0]*x) * np.sin(p[1] * x)


def model_exemplo3(p,x):
    return p[0]*np.cos(p[1]*x) + p[2]*np.sin(p[3]*x)
    #result = optimize.least_squares(model_residue, [1.4, 3, 1.68, 1.3], jac='2-point', method='lm', args=(x, y))

def model_exemplo4(p,x):
    return p[0]*1/(p[1] + np.exp(-x*p[2]))

def model_residue(p, x, y):
    return y - model(p, x)


if __name__ == '__main__':

    data = np.loadtxt('exemplo_4.txt')
    x = data[:, 0]
    y = data[:, 1]
    plt.plot(x, y, '.b')

    result = optimize.least_squares(model_residue, [1.4, 3, 1.68], jac='2-point', method='lm', args=(x,y))

    plt.plot(x, model(result.x, x), 'r')
    plt.show()
