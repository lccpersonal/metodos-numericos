import numpy as np
import math

if __name__ == '__main__':

    F = lambda x, y: x**2 + 2*x + y**2 - 5

    x_=np.asarray([0,0,2,2])
    y_=np.asarray([1,0,0.5,1])

    csi_ = np.asarray([-1/math.sqrt(3),-1/math.sqrt(3),1/math.sqrt(3), 1/math.sqrt(3)])
    zeta_ = np.asarray([1/math.sqrt(3),-1/math.sqrt(3),-1/math.sqrt(3), 1/math.sqrt(3)])

    for csi, zeta in zip(csi_, zeta_):

        phi1 = 0.5*(1 + csi)*0.5*(1 + zeta)
        phi2 = 0.5*(1 - csi)*0.5*(1 + zeta)
        phi3 = 0.5*(1 - csi)*0.5*(1 - zeta)
        phi4 = 0.5*(1 + csi)*0.5*(1 - zeta)

        phi=np.asarray([phi1, phi2, phi3, phi4])

        x = x_[0]*phi[0] + x_[1]*phi[1] + x_[2]*phi[2] + x_[3]*phi[3]
        y = y_[0]*phi[0] + y_[1]*phi[1] + y_[2]*phi[2] + y_[3]*phi[3]


        print('csi:',  csi,'zeta:', zeta)

        dphi_dcsi =np.asarray([[(1/4)*(1+zeta), (-1/4)*(1+zeta), (-1/4)*(1-zeta), (1/4)*(1-zeta)]])
        dphi_dzeta = np.asarray([[(1/4)*(1+csi), (-1/4)*(1-csi), (-1/4)*(1-csi), (1/4)*(1+csi)]])
        
        dx_dcsi = (1/4)*(1+zeta)*x_[0] +  (-1/4)*(1+zeta)*x_[1] + (-1/4)*(1-zeta)*x_[2] + (1/4)*(1-zeta)*x_[3]
        dx_dzeta = (1/4)*(1+csi)*x_[0] +  (1/4)*(1-csi)*x_[1] + (-1/4)*(1-csi)*x_[2] + (-1/4)*(1+csi)*x_[3]

        dy_dcsi = (1/4)*(1+zeta)*y_[0] +  (-1/4)*(1+zeta)*y_[1] + (-1/4)*(1-zeta)*y_[2] + (1/4)*(1-zeta)*y_[3]
        dy_dzeta = (1/4)*(1+csi)*y_[0] +  (1/4)*(1-csi)*y_[1] + (-1/4)*(1-csi)*y_[2] + (-1/4)*(1+csi)*y_[3]

        print('dx_dcsi', dx_dcsi)
        print('dx_dzeta', dx_dzeta)
        print('dy_dcsi', dy_dcsi)
        print('dy_dzeta', dy_dzeta)

        J = np.asarray([[dx_dcsi, dx_dzeta], [dy_dcsi, dy_dzeta]])

        dphi_dnaocartesiano = np.concatenate((dphi_dcsi, dphi_dzeta), axis = 0)

        B = np.matmul(J,dphi_dnaocartesiano )
        print('J', B)
        print('det J', np.linalg.det(J))

        
        

        break
