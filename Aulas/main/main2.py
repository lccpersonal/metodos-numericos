# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 10:16:28 2019

@author: Aluno
"""

import meshio
import matplotlib.pyplot as plt
import matplotlib.collections
import numpy as np
from scipy.sparse.linalg import spsolve
from scipy import sparse, linalg


'''
==============================================
FEM2DHT
==============================================
'''

class fem2dht():
    def __init__(self):
        self.nnodes = 0
        self.nelements = 0
    
    def create_nodes(self, coords):
        self.nodes = coords
        self.nnodes = len(self.nodes)
    
    def create_elements(self, connectivities):
        self.elements = []
        for item in connectivities:
            element = HT3(item)
            self.elements.append(element)
        self.nelements = len(self.elements)
    
    def define_props(self, props):
        for i, element in enumerate(self.elements):
            element.props = props[i]
    
    def create_bc(self, bc_type, nodes, values):
        self.bcs_nodes = nodes
        self.bcs_values = values
    
    def solve(self):
        
        rows = []
        cols = []
        values = []
        
        for element in self.elements:
            r, c, v = element.Kmatrix(self.nodes)
            rows.append(r)
            cols.append(c)
            values.append(v)
        
        rows = np.array(rows, dtype='int').flatten()
        cols = np.array(cols, dtype='int').flatten()
        values = np.array(values, dtype='float').flatten()
        
        Kglobal = sparse.csr_matrix((values, (rows, cols)), shape=((self.nnodes, self.nnodes)))
        Kglobal = Kglobal + Kglobal.T - sparse.diags(Kglobal.diagonal(), dtype='float')
        
        self.Kglobal = Kglobal
        
        
        
        
        
        

class HT3():
    def __init__(self, nodes):
        self.nodes = nodes
        self.props = 0.0
    
    def Kmatrix(self, coords):
        x = coords[self.nodes, 0]
        y = coords[self.nodes, 1]
        
        B = np.zeros((2,3))
        B[0][0] = y[1] - y[2]
        B[0][1] = y[2] - y[0]
        B[0][2] = y[0] - y[1]
        
        B[1][0] = x[2] - x[1]
        B[1][1] = x[0] - x[2]
        B[1][2] = x[1] - x[0]
        
        A = 0.5*(x[0]*y[1] + y[0]*x[2] + x[1]*y[2] - x[2]*y[1] - x[0]*y[2] - x[1]*y[0])
        
        B = (1.0/(2*A)) * B
        
        K = self.props * np.matmul(B.transpose(), B) * A
        
        self.area = A
        self.centroid = [np.mean(x), np.mean(y)]
        
        ind_rows = [self.nodes[0], self.nodes[0], self.nodes[0], self.nodes[1], self.nodes[1], self.nodes[2]]
        ind_cols = [self.nodes[0], self.nodes[1], self.nodes[2], self.nodes[1], self.nodes[2], self.nodes[2]]
        values =   [ K[0,0], K[0,1], K[0,2], K[1,1], K[1,2], K[2,2] ]
        
        return ind_rows, ind_cols, values
        

'''
===============================================
MAIN
===============================================
'''
problem = fem2dht()

coords = [[0.0, 0.0], [2.0, 0.5], [0.0, 1.0], [2.0, 1.0]]
coords = np.array(coords)

connectivities = ([0, 1, 2], [3, 2, 1])

# (1) - Geometry and mesh
problem.create_nodes(coords)
problem.create_elements(connectivities)

nodes = [0, 3]
values = [2, 5]


# (2) - Properties
problem.define_props((5,5))


# (3) - Boundary conditions
problem.create_bc('T', nodes, values)


# (4) - Solve
problem.solve()
'''
# (5) - Postprocessing
problem.plot('T')
'''











