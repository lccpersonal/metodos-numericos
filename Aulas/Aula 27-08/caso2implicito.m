%Caso 2 Impl�cito

clc
clear

c = 0.25;
nt=50;
nx=50;

t = linspace(0,1,nt+1);
x = linspace(0,1,nx+1);

dt = t(2) - t(1);
dx = x(2) - x(1);

%Condi��o inicial
u(:,1) = exp(-(x - 0.5).^2/0.01);
plot(x,u,'-b'); title('Initial Condition');

b = c*dt/(dx^2);

MM = diag(-b*ones(nx-2,1), 1) + diag(-b*ones(nx-2,1), -1) + diag((1+2*b)*ones(nx-1,1) , 0);

MM= inv(MM);


for k = 1:nt %time
    
    u(2:end-1, k+1) = MM*u(2:end-1, k);
    %Boundary Conditions
    u(1, k+1) = 0;
    u(end, k+1) = 0;
end

figure
surf(x,t,u'); shading interp
title(num2str(b))