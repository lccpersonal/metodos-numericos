%Caso 2 Expl�cito

clc
clear

c = 0.25;
nt=1000;
nx=40;

t = linspace(0,1,nt+1);
x = linspace(0,1,nx+1);

dt = t(2) - t(1);
dx = x(2) - x(1);

%Condi��o inicial
u(:,1) = exp(-(x - 0.5).^2/0.01);
plot(x,u,'-b'); title('Initial Condition');


b = c*dt/(dx^2);
for k = 1:nt %time
    for i=2:nx %spatial
        u(i,k+1) = c *(u(i+1,k) - 2*u(i,k) + u(i-1,k))  + u(i,k);
    end
    %Boundary Conditions
    u(1, k+1) = 0;
    u(end, k+1) = 0;
end

figure
surf(x,t,u'); shading interp
title(num2str(b))