%Caso 1 Expl�cito

clc
clear

c = 1.25;
nt=100;
nx=80;

t = linspace(0,1,nt+1);
x = linspace(0,1,nx+1);

dt = t(2) - t(1);
dx = x(2) - x(1);

%Condi��o inicial
u(x < 0.2,1) = 1.0;
u(x >=0.2,1) = 0.0;

plot(x,u,'-b'); title('Initial Condition');

b = c*dt/(2*dx);
for k = 1:nt %time
    for i=2:nx %spatial
        u(i,k+1) = 0.5 * (u(i+1,k) + u(i-1, k) ) - b*(u(i+1,k) - u(i-1,k));
    end
    %Boundary Conditions
    u(1, k+1) = 1;
    u(end, k+1) = 0;
end

figure
surf(x,t,u'); shading interp
title(num2str(b))