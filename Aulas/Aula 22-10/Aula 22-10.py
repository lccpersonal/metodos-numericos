import numpy as np
import matplotlib.pyplot as plt

def triangulo():
    coords = np.asarray([[0, 0], [2, 0.5], [0, 1], [2, 1]])
    elements = np.array([[0, 1, 2], [3, 2, 1]])

    nnodes = len(coords)
    nelements = len(elements)

    K_global = np.zeros((nnodes, nnodes))
    F_global = np.zeros((nnodes, 1))

    props = (5, 5)

    print("Matrices Elements")

    for i, tri in enumerate(elements):
        x = coords[tri, 0]
        y = coords[tri, 1]
        B = np.zeros((2, 3))

        B[0][0] = y[1] - y[2]
        B[0][1] = y[2] - y[0]
        B[0][2] = y[0] - y[1]

        B[1][0] = x[2] - x[1]
        B[1][1] = x[0] - x[2]
        B[1][2] = x[1] - x[0]

        A = 0.5 * (x[0] * y[1] + x[2] * y[0] + x[1] * y[2] - x[2] * y[1] - x[0] * y[2] - x[1] * y[0])

        B = (1 / (2 * A)) * B

        k = props[i] * np.matmul(B.transpose(), B) * A

        K_global[tri[0], [tri[0]]] += k[0][0]
        K_global[tri[0], [tri[1]]] += k[0][1]
        K_global[tri[0], [tri[2]]] += k[0][2]

        K_global[tri[1], [tri[0]]] += k[1][0]
        K_global[tri[1], [tri[1]]] += k[1][1]
        K_global[tri[1], [tri[2]]] += k[1][2]

        K_global[tri[2], [tri[0]]] += k[2][0]
        K_global[tri[2], [tri[1]]] += k[2][1]
        K_global[tri[2], [tri[2]]] += k[2][2]

        print('k:\n', k)

    print('K_glob:\n', K_global)

    w = 1e20

    K_global[0][0] += w
    K_global[3][3] += w

    F_global[0] = 2 * w
    F_global[3] = 5 * w

    T = np.linalg.solve(K_global, F_global)

    print("T:\n", T)

    #plt.triplot(coords[:, 0], coords[:, 1], elements)
    #plt.axis('equal')
    #plt.axis('off')
    #plt.show()

    plt.figure()
    plt.tripcolor(coords[:, 0], coords[:, 1], elements, T, shading='flat')
    plt.colorbar()
    plt.show()

def quadrilatero():

    coords = np.asarray([[0, 0], [2, 0.5], [0, 1], [2, 1]])
    nnodes = len(coords)
    props = 5

    K_global = np.zeros((nnodes, nnodes))
    F_global = np.zeros((nnodes, 1))





if __name__ == '__main__':

    triangulo()
    #quadrilatero()