"""


"""
import meshio
import matplotlib.pylab as plt
import matplotlib.collections
import numpy as np
from scipy.sparse.linalg import spsolve
from scipy import sparse, linalg

class fem2dht:
    def __init__(self):
        self.nnodes = 0
        self.nelements = 0

    def create_nodes(self, coords):
        self.nodes = coords
        self.nnodes = len(self.nodes)

    def create_elements(self, connectivities):
        self.elements = []

        # TO DO -> Classe de Nós
        for item in connectivities:
            element = HT3(item)
            self.elements.append(element)
        self.nelements = len(self.elements)

    def define_props(self, props):
        for i, element in enumerate(self.elements):
            element.props = props[i]

    def create_bc(self, bc_type, nodes, values):
        if bc_type == 'T':
            self.bcs_nodes = nodes
            self.bcs_values = values
        else:
            pass

    def solve(self):
        rows = []
        cols = []
        values = []

        for element in self.elements:
            r, c, v = element.Kmatrix(self.nodes)
            rows.append(r)
            cols.append(c)
            values.append(v)

        rows = np.array(rows, dtype='int').flatten()
        cols = np.array(cols, dtype='int').flatten()
        values = np.array(values, dtype='float').flatten()

        Kglobal = sparse.csr_matrix((values, (rows, cols)), shape=((self.nnodes, self.nnodes)))
        Kglobal = Kglobal + Kglobal.T - sparse.diags(Kglobal.diagonal(), dtype='float')

        self.Kglobal = Kglobal

    def plot(self, bc_type):
        pass

class HT3:
    def __init__(self, nodes):
        self.nodes = nodes
        self.props = 0

    def Kmatrix(self, coords):
        x = coords[self.nodes, 0]
        y = coords[self.nodes, 1]

        self.centroid = [np.mean(x), np.mean(y)]

        B = np.zeros((2, 3))

        B[0][0] = y[1] - y[2]
        B[0][1] = y[2] - y[0]
        B[0][2] = y[0] - y[1]

        B[1][0] = x[2] - x[1]
        B[1][1] = x[0] - x[2]
        B[1][2] = x[1] - x[0]

        self.A = 0.5 * (x[0] * y[1] + x[2] * y[0] + x[1] * y[2] - x[2] * y[1] - x[0] * y[2] - x[1] * y[0])
        B = (1 / (2 * self.A)) * B
        k = self.props * np.matmul(B.transpose(), B) * self.A

        index_ind_rows = [0, 0, 0, 1, 1, 2]
        index_ind_cols = [0, 1, 2, 1, 2, 2]

        ind_rows = self.nodes[index_ind_rows]
        ind_cols = self.nodes[index_ind_cols]

        values = [k[0, 0], k[0, 1], k[0, 2], k[1, 1], k[1, 2], k[2, 2]]

        return ind_rows, ind_cols, values

if __name__ == '__main__':
    problem = fem2dht()

    coords = np.asarray([[0, 0], [2, 0.5], [0, 1], [2, 1]])
    connectivities = np.array([[0, 1, 2], [3, 2, 1]])
    nodes = [0, 3]
    values = [2, 5]

    # (1) Geometry and Mesh
    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    # (2) Properties
    problem.define_props((5, 5))

    # (3) Boundary Conditions
    problem.create_bc('T', nodes, values)

    # (4) Solve
    problem.solve()

    # (5) Postprocessing
    problem.plot('T')
