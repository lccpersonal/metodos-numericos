Point(1) = {0, 0, 0, 1.0};
Point(2) = {1, 0, 0, 1.0};
Point(3) = {1, 1, 0, 1.0};
Point(4) = {0, 1, 0, 1.0};
Point(5) = {0.5, 0.5, 0, 1.0};
Point(6) = {0.6, 0.5, 0, 1.0};
Point(7) = {0.4, 0.5, 0, 1.0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Circle(5) = {6, 5, 7};
Circle(6) = {7, 5, 6};

Line Loop(1) = {1, 2, 3, 4};
Line Loop(2) = {5, 6};

Plane Surface(1) = {1, 2};
//+
Plane Surface(7) = {1, 2};