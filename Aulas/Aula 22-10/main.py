#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 20:42:29 2019

@author: ricardo
"""

import meshio
import matplotlib.pyplot as plt
import matplotlib.collections
import numpy as np

def triquadplot(x, y, quatrangles, ax=None, **kwargs):
    if not ax: ax=plt.gca()
    xy = np.c_[x,y]
    verts=xy[quatrangles]
    pc = matplotlib.collections.PolyCollection(verts, **kwargs)
    ax.add_collection(pc)
    ax.autoscale()

if __name__ == "__main__":

    mesh = meshio.read('mesh_tri1.msh')

    x = mesh.points[:, 0]
    y = mesh.points[:, 1]

    plt.close('all')
    plt.figure()

    if 'triangle' in mesh.cells:
        tri = mesh.cells['triangle']
        triquadplot(x, y, tri, ax=None, facecolor='lightblue', edgecolor='midnightblue')
        plt.axis('equal')
        plt.axis('off')
        plt.show()

    if 'quad' in mesh.cells:
        quad = mesh.cells['quad']
        triquadplot(x, y, quad, ax=None, facecolor='lightblue', edgecolor='midnightblue')
        plt.axis('equal')
        plt.axis('off')
        plt.show()

    print(mesh)