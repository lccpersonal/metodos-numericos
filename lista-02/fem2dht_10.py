"""


"""
import meshio
import matplotlib.pylab as plt
import matplotlib.collections
import numpy as np
from scipy.sparse.linalg import spsolve
from scipy import sparse


class fem2dht():
    def __init__(self, elem_type: str):
        self.nnodes = 0
        self.nelements = 0
        self.elem_type = elem_type

    def create_nodes(self, coords: np.array):
        self.nodes = coords
        self.nnodes = len(self.nodes)

    def create_elements(self, connectivities: np.array):
        self.elements = []
        self.connectivities = connectivities

        if self.elem_type == 'tri':
            for nodes_element in connectivities:
                element = HT3(nodes_element)
                self.elements.append(element)
            self.nelements = len(self.elements)

        elif self.elem_type == 'quad':
            for nodes_element in connectivities:
                element = HT3_2(nodes_element)
                self.elements.append(element)
            self.nelements = len(self.elements)


    def define_props(self, props: float):
        for i, element in enumerate(self.elements):
            element.props = props

    def create_bc(self, bc_type: str, nodes: list, values: list):

        if bc_type == 'T':
            self.bc_dirichlet_nodes = nodes
            self.bc_dirichlet_values = values


    def solve(self):

        rows = []
        cols = []
        values = []


        for element in self.elements:
            r, c, v = element.Kmatrix(self.nodes)
            rows.append(r)
            cols.append(c)
            values.append(v)


        rows = np.array(rows, dtype='int').flatten()
        cols = np.array(cols, dtype='int').flatten()
        values = np.array(values, dtype='float').flatten()

        Kglobal = sparse.csr_matrix((values, (rows, cols)), shape=((self.nnodes, self.nnodes)))
        Kglobal = Kglobal + Kglobal.T - sparse.diags(Kglobal.diagonal(), dtype='float')

        self.Kglobal = Kglobal
        # 1) Montagem do vetor fglobal
        fglobal = np.zeros(self.nnodes)

        # 2) Aplicação das BCs no vetor fglobal
        # 3) Aplicar as condições de contorno na matriz Kglobal
        w = 1e20
        nbcs = len(self.bc_dirichlet_nodes)
        for k in range(nbcs):
            node = self.bc_dirichlet_nodes[k]
            fglobal[node] = self.bc_dirichlet_values[k] * w
            Kglobal[node, node] += w

        self.Kglobal = Kglobal
        self.fglobal = fglobal

        # 5) Resolver o problema
        self.T = spsolve(Kglobal, fglobal)

        if self.elem_type == 'tri':
            # 6) Encontrar o Fluxo
            flux = []
            el_centroids = []
            for n, element in enumerate(self.elements):
                B = element.B
                n = element.nodes
                T_ = self.T[n]
                f = -element.props*np.matmul(B, T_)  # Cálculo do Fluxo
                flux.append(f)
                el_centroids.append(element.centroid)

            self.flux = np.array(flux)
            self.el_centroids = np.array(el_centroids)


    def plot(self):
        if self.elem_type == 'tri':

            plt.figure()
            plt.tricontour(self.nodes[:, 0], self.nodes[:, 1], self.connectivities, self.T, colors='k')
            plt.tripcolor(self.nodes[:, 0], self.nodes[:, 1], self.connectivities, self.T, shading='gouraud', linewidths=3)
            plt.colorbar()
            plt.legend('Temperatura')
            plt.axis('equal')
            plt.show()

            plt.figure()
            plt.tripcolor(self.nodes[:, 0], self.nodes[:, 1], self.connectivities, facecolors=self.flux[:, 0], shading='flat', edgecolors='none')
            plt.colorbar()
            plt.legend('Fluxo x')
            plt.axis('equal')
            plt.show()

            plt.figure()
            plt.tripcolor(self.nodes[:, 0], self.nodes[:, 1], self.connectivities, facecolors=self.flux[:,1], shading='flat', edgecolors='none')
            plt.colorbar()
            plt.legend('Fluxo y')
            plt.axis('equal')
            plt.show()

        elif self.elem_type == 'quad':
            plt.figure()
            conec = np.concatenate((self.connectivities[:, :-1], self.connectivities[:, (0, 2, 3)]), axis = 0)
            plt.tripcolor(self.nodes[:, 0], self.nodes[:, 1], conec, self.T, vmin=0, vmax =10,  shading='gouraud')
            plt.colorbar()
            plt.legend('Quadratico')
            plt.axis('equal')
            plt.show()



class HT3:
    def __init__(self, nodes: np.array):
        self.nodes = nodes
        self.props = 0

    def Kmatrix(self, coords: np.array):
        x = coords[self.nodes, 0]
        y = coords[self.nodes, 1]

        self.centroid = [np.mean(x), np.mean(y)]

        B = np.zeros((2, 3))

        B[0][0] = y[1] - y[2]
        B[0][1] = y[2] - y[0]
        B[0][2] = y[0] - y[1]

        B[1][0] = x[2] - x[1]
        B[1][1] = x[0] - x[2]
        B[1][2] = x[1] - x[0]

        self.A = 0.5 * (x[0] * y[1] + x[2] * y[0] + x[1] * y[2] - x[2] * y[1] - x[0] * y[2] - x[1] * y[0])
        B = (1 / (2 * self.A)) * B
        self.B = B
        k = self.props * np.matmul(B.transpose(), B) * self.A

        index_ind_rows = [0, 0, 0, 1, 1, 2]
        index_ind_cols = [0, 1, 2, 1, 2, 2]

        ind_rows = self.nodes[index_ind_rows]
        ind_cols = self.nodes[index_ind_cols]

        values = [k[0, 0], k[0, 1], k[0, 2], k[1, 1], k[1, 2], k[2, 2]]

        return ind_rows, ind_cols, values


class HT3_2():
    def __init__(self, nodes):
        self.nodes = nodes
        self.props = 0.0

    def Kmatrix(self, coords):
        x = coords[self.nodes, 0]
        y = coords[self.nodes, 1]
        mycoords = [x, y]
        mycoords = np.matrix((mycoords))
        mycoords = mycoords.transpose()

        qsi = [-1 / np.sqrt(3), 1 / np.sqrt(3)]
        eta = [-1 / np.sqrt(3), 1 / np.sqrt(3)]
        w1 = 1
        w2 = 1

        K = np.zeros((4, 4))

        for i in range(2):
            for j in range(2):
                m = qsi[i]
                d = eta[j]

                J = np.zeros((2, 2))
                J[0, 1] = 0.125 * d - 0.375
                J[1, 0] = 1.0
                J[1, 1] = 0.125 * m + 0.125

                detJ = -0.125 * d + 0.375

                Jinv = np.zeros((2, 2))
                Jinv[0, 0] = (1 + m) / (3 - d)
                Jinv[0, 1] = 1
                Jinv[1, 0] = 8 / (d - 3)

                GN = np.zeros((2, 4))
                GN[0, 0] = 0.25 * (d - 1)
                GN[0, 1] = 0.25 * (1 - d)
                GN[0, 2] = 0.25 * (1 + d)
                GN[0, 3] = 0.25 * (-d - 1)

                GN[1, 0] = 0.25 * (m - 1)
                GN[1, 1] = 0.25 * (-m - 1)
                GN[1, 2] = 0.25 * (1 + m)
                GN[1, 3] = 0.25 * (1 - m)

                B = np.matmul(Jinv, GN)

                K += self.props * detJ * np.matmul(B.transpose(), B)

        A = 0.5 * (x[0] * y[1] + y[0] * x[2] + x[1] * y[2] - x[2] * y[1] - x[0] * y[2] - x[1] * y[0])
        self.area = A
        # pra que isso?
        self.centroid = [np.mean(x), np.mean(y)]

        ind_rows = [self.nodes[0], self.nodes[0], self.nodes[0], self.nodes[0], self.nodes[1], self.nodes[1],
                    self.nodes[1], self.nodes[2], self.nodes[2], self.nodes[3]]
        ind_cols = [self.nodes[0], self.nodes[1], self.nodes[2], self.nodes[3], self.nodes[1], self.nodes[2],
                    self.nodes[3], self.nodes[2], self.nodes[3], self.nodes[3], ]
        values = [K[0, 0], K[0, 1], K[0, 2], K[0, 3], K[1, 1], K[1, 2], K[1, 3], K[2, 2], K[2, 3], K[3, 3]]

        return ind_rows, ind_cols, values


class HT4:

    def __init__(self, nodes: np.array):
        self.nodes = nodes
        self.props = 0.0

    def Kmatrix(self, coords: np.array):

        x_ = np.array(coords[self.nodes])
        self.centroid = [np.mean(x_[:, 0]), np.mean(x_[:, 1])]

        dp1dxi = lambda qsi, eta:  (1 / 4) * (eta - 1)
        dp2dxi = lambda qsi, eta:  (1 / 4) * (1 - eta)
        dp3dxi = lambda qsi, eta:  (1 / 4) * (1 + eta)
        dp4dxi = lambda qsi, eta:  (1 / 4) * (-1 - eta)

        dp1deta = lambda qsi, eta:  (1 / 4) * (1 - qsi)
        dp2deta = lambda qsi, eta:  (1 / 4) * (-1 - qsi)
        dp3deta = lambda qsi, eta:  (1 / 4) * (1 + qsi)
        dp4deta = lambda qsi, eta:  (1 / 4) * (1 - qsi)

        GN = lambda qsi, eta: \
            np.array(  [[dp1dxi(qsi, eta),  dp2dxi(qsi, eta),  dp3dxi(qsi, eta),  dp4dxi(qsi, eta)],
                        [dp1deta(qsi, eta), dp2deta(qsi, eta), dp3deta(qsi, eta), dp4deta(qsi, eta)]])

        Jac = lambda qsi, eta, x_: \
            np.matmul(GN(qsi, eta), x_)

        qsi = [-1 / np.sqrt(3), 1 / np.sqrt(3)]
        eta = [-1 / np.sqrt(3), 1 / np.sqrt(3)]
        A = 4

        # integração gauss
        K = np.zeros((4, 4))
        for QSI in qsi:
            for ETA in eta:
                jacobiana = Jac(QSI, ETA, x_)
                B = np.matmul(np.linalg.inv(jacobiana), GN(QSI, ETA))
                K = K + self.props * np.matmul(B.transpose(), B) * np.linalg.det(jacobiana) * A

        index_ind_rows = [0, 0, 0, 0, 1, 1, 1, 2, 2, 3]
        index_ind_cols = [0, 1, 2, 3, 1, 2, 3, 2, 3, 3]

        ind_rows = self.nodes[index_ind_rows]
        ind_cols = self.nodes[index_ind_cols]

        values = []
        for i, j in zip(index_ind_rows, index_ind_cols):
            values.append(K[i, j])

        return ind_rows, ind_cols, values


if __name__ == '__main__':

    problem = fem2dht('tri')

    mesh = meshio.read('ex1_mesh1_tri.msh')
    coords = np.array(mesh.points[:, 0:2])
    connectivities = mesh.cells['triangle']

    # Implementação do elemento Q4
    # connectivities = ([0, 1, 2, 3])

    # (1) - Geometry and mesh
    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    # TODO: alterar para os nós da malha importada
    nodes = [0, 2, 4]
    values = [10, 5, 7.5]

    # (2) - Properties
    problem.define_props(5)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()



