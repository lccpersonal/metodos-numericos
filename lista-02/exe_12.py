
import numpy as np
from typing import Tuple
import matplotlib.pyplot as plt
from fem2dht_12 import *
import meshio

def define_bc(coords):
    p1 =  [0, 1]
    vp1 = [0, 0]

    y_0 = np.where((coords[:, 0] == 0))
    #[0, 0.2, 0.4, 0.6, 0.8, 1]
    dof = [2*ndof, 2*ndof + 1]

    return nodes, values


def test_tri():
    problem = fem2dht('tri')

    coords = np.array([[0, 0], [0.5, 0], [1, 1], [0.5, 1]])
    connectivities = np.array([[0, 1, 3], [1, 2, 3]])

    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    nodes = [0, 2]
    values = np.array([[0, 0],
                       [2000,  4000]])

    problem.define_props(2.1e9, 0.3)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()

    return problem

def test_quad():

    problem = fem2dht('quad')

    coords = np.array([[0, 1], [0, 0 ], [2, 0.5], [2, 1]])

    connectivities = np.array([[0, 1, 2, 3]])

    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    nodes =  [0]
    values = np.array([[0, 0]])

    problem.define_props(3.3e7, 0.3)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()

    return problem


def triangular():
    problem = fem2dht('tri')
    ndof = 2
    mesh = meshio.read('ex1_mesh1_tri.msh')

    coords = np.array(mesh.points[:, 0:2])
    connectivities = mesh.cells['triangle']

    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    # TODO: Colocar em prática o esquema dos pontos
    # Buscar as posições dos pontos de interesse
    #nodes, values = define_bc()

    nodes = [2*2, 0]
    values = np.array([[2000, 4000],
                       [   0,    0]])

    problem.define_props(2.1e9, 0.3)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()

    return problem

def quadrilateral():

    problem = fem2dht('quad')

    mesh = meshio.read('mesh_quad1.msh')

    coords = np.array(mesh.points[:, 0:2])
    connectivities = mesh.cells['quad']

    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    x_0 = np.where(coords[:, 0] == 0)[0]
    x_1 = np.where(coords[:, 0] == 1)[0]

    nodes = x_0.tolist() + x_1.tolist() + [4]
    values = (10*np.ones_like(x_0)).tolist() + np.ones_like(x_1).tolist() + [0]

    problem.define_props(5)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()

    return problem


if __name__ == '__main__':
    #p0 = test_tri()
    p4 = test_quad()
    #p1 = triangular()
    #p2 = quadrilateral()