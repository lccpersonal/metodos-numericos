from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse.linalg import spsolve
from scipy import sparse

if __name__ == '__main__':
    # Properties of the system
    rho = 1  # Kg/m**3
    c = 100  # J Kg**-1 K**-1
    k = 1  # W m**-2 K**-1
    A = 1  # m**2

    nt = 30
    nx = 100

    # Discretization
    T = np.linspace(0, 30, num=nt + 1)
    X = np.linspace(0,  1, num=nx + 1)
    dt = T[1] - T[0]
    dx = X[1] - X[0]
    L = 1/nx  # m

    # Boundary Conditions
    q = np.array([0 if t_ > 10 else 1 for t_ in T])

    Temp = np.zeros((len(X), 1, len(T)))
    Temp[:, :, 0] = 1
    C = rho*L*c*np.array([[1/3, 1/6], [1/6, 1/3]])
    K = (1/L) * np.array([[1, -1], [-1, 1]])
    F = np.zeros_like(Temp)

    for i, q_ in enumerate(q):
        F[0, :, i] = q_


    iG_K = np.zeros((4, nx))
    jG_K = np.zeros((4, nx))
    vG_K = np.zeros((4, nx))

    iG_C = np.zeros((4, nx))
    jG_C = np.zeros((4, nx))
    vG_C = np.zeros((4, nx))

    for n in range(nx):
        i = [n, n+1, n, n+1]
        j = [n, n, n+1, n+1]

        v_K = K.reshape((1, 4))
        iG_K[:, n] = i
        jG_K[:, n] = j
        vG_K[:, n] = v_K

        v_C = C.reshape((1, 4))
        iG_C[:, n] = i
        jG_C[:, n] = j
        vG_C[:, n] = v_C

    K = sparse.csr_matrix((vG_K.flatten(), (iG_K.flatten(), jG_K.flatten())), shape=((len(X), len(X))))
    C = sparse.csr_matrix((vG_C.flatten(), (iG_C.flatten(), jG_C.flatten())), shape=((len(X), len(X))))

    aux1 = C + dt * K
    for k in range(nt):
        aux2 = C.dot(Temp[:, :, k]) + dt*F[:, :, k+1]
        Temp[:, :, k+1] = np.expand_dims(spsolve(aux1, aux2), axis=1)

    for k in range(nt):
        if k < 10:
            plt.plot(X, Temp[:, 0, k], 'r')
        else:
            plt.plot(X, Temp[:, 0, k], 'b')

    plt.show()
