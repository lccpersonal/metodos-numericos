

import numpy as np
from typing import Tuple
import matplotlib.pyplot as plt
from func import *


if __name__ == '__main__':

    vertices = [(0, 0), (2, 1), (3, 2), (-1, 2)]
    vertices_ = [(-1, -1), (1, -1), (1, 1), (-1, 1)]

    xx = lambda xi, eta: (3/2)*xi + 0.5*eta*xi + 1
    yy = lambda xi, eta: (1/4)*(5 + 3*eta + xi + -xi*eta)

    Int = lambda x, y: x*x + y*y

    plt.figure()
    for i in range(len(vertices)):
        plt.scatter(vertices[i][0], vertices[i][1], c='b')
        plt.scatter(vertices_[i][0], vertices_[i][1], c='g')
        plt.scatter(xx(vertices_[i][0], vertices_[i][1]), yy(vertices_[i][0], vertices_[i][1]), c='r')
    plt.show()

    Answer = []

    for degree in [1, 2, 3, 4]:
        points, weights = gauss_legendre_2D(degree)

        A = 0

        for p in range(points.shape[0]):
            x = xx(points[p, 0], points[p, 1])
            y = yy(points[p, 0], points[p, 1])

            A  += Int(x, y) * weights[p]

        Answer.append(A*(4.5/4))  # Fator de Domínio

    print(Answer)


