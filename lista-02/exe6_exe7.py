

import numpy as np
from typing import Tuple
import matplotlib.pyplot as plt
from scipy.special import erf
import scipy
from func import *

int_erf = lambda x: x*erf(x) + scipy.exp(-x**2)/scipy.sqrt(scipy.pi)

if __name__ == '__main__':
    limits = [0, 5]
    n_quadrature_points = list(range(1, 8))

    print('Exact Value: ', int_erf(limits[1]) - int_erf(limits[0]))

    for n in n_quadrature_points:
        points, weights = gauss_legendre_1D(n)
        points = (points + 1)*2.5
        s = 0
        for p, w in zip(points, weights):
            s += w*erf(p)
        s = s*2.5
        print('Approximation gauss-legendre', n, ' : ', s)

    for n in n_quadrature_points:
        if n == 1:
            continue
        points, weights = gauss_lobatto_1D(n)
        points = (points + 1)*2.5
        s = 0
        for p, w in zip(points, weights):
            s += w*erf(float(p))
        s = s*2.5
        print('Approximation gauss-lobatto', n, ' : ', s)