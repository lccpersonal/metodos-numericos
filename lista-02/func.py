
import numpy as np
from typing import Tuple
import matplotlib.pyplot as plt
from sympy.integrals.quadrature import gauss_lobatto

def gauss_legendre_1D(degree: int) -> Tuple[np.array, np.array]:

    points, weights = np.polynomial.legendre.leggauss(degree)

    return points, weights



def gauss_legendre_2D(degree: int) -> Tuple[np.array, np.array]:

    _points, _weights = np.polynomial.legendre.leggauss(degree)

    points = []
    weights = []

    for x in range(_points.size):
        for y in range(_points.size):
            points.append((_points[x], _points[y]))
            weights.append(_weights[x]*_weights[y])

    points = np.array(points)
    weights = np.array(weights)

    return points, weights



def gauss_lobatto_1D(degree: int) -> Tuple[np.array, np.array]:

    points, weights = gauss_lobatto(degree, 8)

    points = np.array(points)
    weights = np.array(weights)

    return points, weights


if __name__ == '__main__':

    a, b = gauss_legendre_2D(4)
    #a, b = gauss_lobatto_1D(4)
    #c = np.unique(a, axis=0)
    print(a)
    #print(c)

    #plt.figure()
    #for item in range(a.shape[0]):
    #    plt.scatter(a[item, 0], a[item, 1])

    #plt.show()
