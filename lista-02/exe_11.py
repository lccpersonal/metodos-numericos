
from scipy.spatial import distance
import numpy as np
from typing import Tuple
import matplotlib.pyplot as plt
import math

def problem_mesh():
    nnode = 9
    nel = 15

    gcoord = np.zeros((nnode, 2))
    gcoord[0, :] = 0.0, 0.0
    gcoord[1, :] = 0.3, 0.0
    gcoord[2, :] = 0.6, 0.0
    gcoord[3, :] = 0.9, 0.0
    gcoord[4, :] = 1.2, 0.0
    gcoord[5, :] = 0.15, 0.25981
    gcoord[6, :] = 0.45, 0.25981
    gcoord[7, :] = 0.75, 0.25981
    gcoord[8, :] = 1.05, 0.25981

    nodes = np.zeros((nel, 2))
    nodes[0, :] = 0, 1
    nodes[1, :] = 1, 2
    nodes[2, :] = 2, 3
    nodes[3, :] = 3, 4
    nodes[4, :] = 0, 5
    nodes[5, :] = 1, 5
    nodes[6, :] = 1, 6
    nodes[7, :] = 2, 6
    nodes[8, :] = 2, 7
    nodes[9, :] = 3, 7
    nodes[10, :] = 3, 8
    nodes[11, :] = 4, 8
    nodes[12, :] = 5, 6
    nodes[13, :] = 6, 7
    nodes[14, :] = 7, 8

    return gcoord, nodes


def feeldof(nd, nnel, ndof):
    k=0
    for i in range(nnel):
        start = nd[i]*ndof
        for j in range(ndof):
            index[k] = start + j
            k = k + 1
    return index

def fetruss2(el, leng, area, rho, beta, ipt):
    c = math.cos(beta)
    s = math.sin(beta)
    k = np.array([[c*c,   c*s, -c*c, -c*s],
                  [c*s,   s*s, -c*s, -s*s],
                  [-c*c, -c*s,  c*c,  c*s],
                  [-c*s, -s*s,  c*s,  s*s]])

    k = k*(area*el/leng)

    mm = c * c + s * s
    if ipt == 1:
        m = np.array([[2*mm,     0,   mm,    0],
                      [   0,  2*mm,    0,   mm],
                      [  mm,     0, 2*mm,    0],
                      [   0,    mm,    0, 2*mm]])

        m = (rho*area*leng/6)*m

    else:
        m = np.diag([mm, mm, mm, mm])*(rho*area*leng/2)

    return k, m

def feasmbl1(kk, k, index):
    edof = len(index)

    for i in range(edof):
        ii = index[i]
        for j in range(edof):
            jj = index[j]
            kk[int(ii), int(jj)] += k[i,j]

    return kk


def feaplyc2(kk, ff, bcdof, bcval):
    n = len(bcdof)
    sdof = kk.shape[0]

    for i in range(n):
        for j in range(sdof):
            kk[bcdof[i], j] = 0

        kk[bcdof[i], bcdof[i]] = 1
        ff[bcdof[i]] = bcval[i]
    return kk, ff

if __name__ == '__main__':

    nel = 15
    nnel = 2
    ndof = 2
    nnode = 9
    sdof = nnode * ndof

    E = 210e9
    A = 0.000003142

    props = np.array([E, A])

    gcoord, nodes = problem_mesh()

    # Boundary conditions
    bcdoff = [0, 1, 9]  # Boundary Conditions for dof 1, 2, 10
    bcval = [0, 0, 0]

    ff =      np.zeros((sdof, 1))
    kk =      np.zeros((sdof, sdof))
    index =   np.zeros((nnel * ndof, 1))
    elforce = np.zeros((nnel * ndof, 1))
    eldisp =  np.zeros((nnel * ndof, 1))
    k =       np.zeros((nnel * ndof, nnel * ndof))
    stress =  np.zeros((nel, 1))

    ff[13] = -4000
    ff[15] = -2000
    # i = 4
    for i in range(nel):
        nd = nodes[i]

        x1, y1 = gcoord[int(nd[0]), :]
        x2, y2 = gcoord[int(nd[1]), :]

        leng = math.sqrt((x2-x1)**2 + (y2-y1)**2)

        if x2 - x1 == 0:
            beta = math.atan(1)
        else:
            beta = math.atan((y2-y1) / (x2-x1))

        el = E
        area = A

        index = feeldof(nd, nnel, ndof)
        k, _ = fetruss2(el, leng, area, 0, beta, 1)
        kk = feasmbl1(kk, k, index)

    kk, ff = feaplyc2(kk, ff, bcdoff, bcval)


    disp = np.linalg.solve(kk, ff)

    for ie1 in range(nel):
        nd = nodes[ie1, :]

        x1, y1 = gcoord[int(nd[0]), :]
        x2, y2 = gcoord[int(nd[1]), :]

        leng = math.sqrt(abs((x2-x1)**2 + (y2-y1)**2))

        if x2 - x1 == 0:
            beta = math.atan(1)
        else:
            beta = math.atan((y2-y1) / (x2-x1))

        el = E
        area = A

        index = feeldof(nd, nnel, ndof)
        k, m = fetruss2(el, leng, area, 0, beta, 1)

        for i in range(nnel*ndof):
            eldisp[i] = disp[int(index[i])]

        elforce = np.matmul(k, eldisp)
        stress[ie1] = math.sqrt(elforce[0]**2 + elforce[1]**2)/area

        if ((x2-x1)*elforce[2]) < 0:
            stress[ie1] = -stress[ie1]

        num = np.linspace(0, sdof - 1 , num=sdof).reshape(sdof, 1)
        disp1 = np.concatenate((num, disp), axis=1)

        numm = np.linspace(0, nel - 1, num=nel).reshape(nel, 1)
        stresses = np.concatenate((numm, stress), axis=1)


    for ie in range(nel):
        n = nodes[ie]
        n_ = n + 1
        index = np.array([n_[0]*2 - 1, n_[0]*2, n_[1]*2 - 1, n_[1]*2]) - 1

        edisp = []
        edisp1 = []

        for i in range(nnel*ndof):
            edisp.append( disp[int(index[i])] )
            edisp1.append( disp1[int(index[i])] )

        x0 = [gcoord[int(nodes[ie, 0]), 0], gcoord[int(nodes[ie, 1]), 0] ]
        y0 = [gcoord[int(nodes[ie, 0]), 1], gcoord[int(nodes[ie, 1]), 1] ]

        x = [x0[0] + edisp[0], x0[1] + edisp[2]]
        y = [y0[0] + edisp[1], y0[1] + edisp[3]]

        plt.plot(x0, y0, 'b')
        plt.plot(x, y, 'r')

    plt.show()

