
import numpy as np
import matplotlib.pyplot as plt



if __name__ == '__main__':

    phi1 = lambda x: (1 / 48) * (-x ** 3 + 6 * x ** 2 - 8 * x)
    phi2 = lambda x: (1 / 16) * (x ** 3 - 4 * x ** 2 - 4 * x + 16)
    phi3 = lambda x: (1 / 16) * (-x ** 3 + 2 * x ** 2 + 8 * x)
    phi4 = lambda x: (1 / 48) * (x ** 3 - 4 * x)

    x = np.linspace(-2, 4, num=20)

    y = np.vectorize(phi1)(x)
    plt.plot(x, y)
    y = np.vectorize(phi2)(x)
    plt.plot(x, y)
    y = np.vectorize(phi3)(x)
    plt.plot(x, y)
    y = np.vectorize(phi4)(x)
    plt.plot(x, y)
    plt.show()

    u = np.array([1, 0, 2, 3])

    func = lambda x: 1*phi1(x) + 2*phi3(x) + 3*phi4(x)

    y = np.vectorize(func)(x)
    plt.plot(x, y)
    plt.show()

