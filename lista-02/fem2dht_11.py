"""


"""
import meshio
import matplotlib.pylab as plt
import matplotlib.collections
import numpy as np
from scipy.sparse.linalg import spsolve
from scipy import sparse, linalg


class fem2dht():
    def __init__(self, elem_type: str, ndof):
        self.nnodes = 0
        self.nelements = 0
        self.elem_type = elem_type
        self.ndof = ndof

    def create_nodes(self, coords: np.array):
        self.nodes = coords
        self.nnodes = len(self.nodes)

    def create_elements(self, connectivities: np.array):
        self.elements = []
        self.connectivities = connectivities
        for nodes_element in connectivities:
            element = HT3(nodes_element)
            self.elements.append(element)
        self.nelements = len(self.elements)

    def define_props(self, props: list):
        for i, element in enumerate(self.elements):
            element.E = props[0]
            element.A = props[1]

    def create_bc(self, bc_type: str, nodes: list, values: list):

        if bc_type == 'D':
            self.bc_desloc_dof = nodes
            self.bc_desloc_values = values
        if bc_type == 'F':
            self.bc_force_dof = nodes
            self.bc_force_values = values


    def solve(self):

        rows = []
        cols = []
        values = []


        for element in self.elements:
            r, c, v = element.Kmatrix(self.nodes)
            rows.append(r)
            cols.append(c)
            values.append(v)


        rows = np.array(rows, dtype='int').flatten()
        cols = np.array(cols, dtype='int').flatten()
        values = np.array(values, dtype='float').flatten()

        Kglobal = sparse.csr_matrix((values, (rows, cols)), shape=((self.nnodes, self.nnodes)))
        Kglobal = Kglobal + Kglobal.T - sparse.diags(Kglobal.diagonal(), dtype='float')

        self.Kglobal = Kglobal
        # 1) Montagem do vetor fglobal
        fglobal = np.zeros(self.nnodes)

        # 2) Aplicação das BCs no vetor fglobal
        # 3) Aplicar as condições de contorno na matriz Kglobal
        w = 1e20
        nbcs = len(self.bc_desloc_dof)
        for k in range(nbcs):
            node = self.bc_desloc_dof[k]
            fglobal[node] = self.bc_desloc_values[k] * w
            Kglobal[node, node] += w

        self.Kglobal = Kglobal
        self.fglobal = fglobal

        # 5) Resolver o problema
        self.T = spsolve(Kglobal, fglobal)



    def plot(self):
        if self.elem_type == 'tri':

            plt.figure()
            plt.tricontour(self.nodes[:, 0], self.nodes[:, 1], self.connectivities, self.T, colors='k')
            plt.tripcolor(self.nodes[:, 0], self.nodes[:, 1], self.connectivities, self.T, shading='gouraud', linewidths=3)
            plt.colorbar()
            plt.legend('Temperatura')
            plt.axis('equal')
            plt.show()





class HT3:
    def __init__(self, nodes):
        self.nodes = nodes
        self.E = 0
        self.A = 0

    def Kmatrix(self, coords):
        x = coords[self.nodes, 0]
        y = coords[self.nodes, 1]
        self.centroid = [np.mean(x), np.mean(y)]

        dpdx = [y[1] - y[2], y[2] - y[0], y[0] - y[1]]
        dpdy = [x[2] - x[1], x[0] - x[2], x[1] - x[0]]

        B = np.array([[dpdx[0],       0, dpdx[1],       0, dpdx[2],       0],
                      [      0, dpdy[0],       0, dpdy[1],       0, dpdy[2]],
                      [dpdy[0], dpdx[0], dpdy[1], dpdx[1], dpdy[2]], dpdy[2]])

        self.B = B

        aux = np.matmul(B.transpose(), self.props)
        k = np.matmul(aux, B)

        index_ind_rows = [0, 0, 0, 1, 1, 2]
        index_ind_cols = [0, 1, 2, 1, 2, 2]

        ind_rows = self.nodes[index_ind_rows]
        ind_cols = self.nodes[index_ind_cols]

        values = [k[0, 0], k[0, 1], k[0, 2], k[1, 1], k[1, 2], k[2, 2]]

        return ind_rows, ind_cols, values





if __name__ == '__main__':

    problem = fem2dht('tri')

    mesh = meshio.read('ex1_mesh1_tri.msh')
    coords = np.array(mesh.points[:, 0:2])
    connectivities = mesh.cells['triangle']

    # Implementação do elemento Q4
    # connectivities = ([0, 1, 2, 3])

    # (1) - Geometry and mesh
    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    # TODO: alterar para os nós da malha importada
    nodes = [0, 2, 4]
    values = [10, 5, 7.5]

    # (2) - Properties
    problem.define_props(5)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()



