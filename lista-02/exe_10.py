
import numpy as np
from typing import Tuple
import matplotlib.pyplot as plt
from fem2dht_10 import *
import meshio

def triangular():
    problem = fem2dht('tri')

    mesh = meshio.read('ex1_mesh1_tri.msh')

    coords = np.array(mesh.points[:, 0:2])
    connectivities = mesh.cells['triangle']

    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    x_0 = np.where(coords[:, 0] == 0)[0]
    x_1 = np.where(coords[:, 0] == 1)[0]

    nodes = x_0.tolist() + x_1.tolist() + [4]
    values = (10 * np.ones_like(x_0)).tolist() + np.ones_like(x_1).tolist() + [0]

    problem.define_props(5)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()

    return problem

def quadrilateral():

    problem = fem2dht('quad')

    mesh = meshio.read('mesh_quad1.msh')

    coords = np.array(mesh.points[:, 0:2])
    connectivities = mesh.cells['quad']

    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    x_0 = np.where(coords[:, 0] == 0)[0]
    x_1 = np.where(coords[:, 0] == 1)[0]
    y_0 = np.where(coords[:, 1] == 0)[0]
    y_1 = np.where(coords[:, 1] == 1)[0]

    nodes = x_0.tolist() + x_1.tolist() + y_0.tolist() + y_1.tolist() + [4]
    values = (10*np.ones_like(x_0)).tolist() + np.ones_like(x_1).tolist() + \
                 np.zeros_like(y_0).tolist() + np.zeros_like(y_1).tolist() + [0]

    problem.define_props(5)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()

    return problem


def test_quad():

    problem = fem2dht('quad')

    coords = np.array([[0, 0], [0, 0.5], [0, 1], [0.5, 0], [0.5, 0.5], [0.5, 1], [1, 0],
                      [1, 0.5], [1, 1]])

    connectivities = np.array([[0, 1, 4, 3], [1, 2, 5, 4], [3, 4, 7, 6], [4, 5, 8, 7]])

    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    nodes =  [0, 8]
    values = [1, 10]

    problem.define_props(5)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()

    return problem


if __name__ == '__main__':

    p1 = triangular()
    p2 = quadrilateral()
    #p3 = test_quad()

