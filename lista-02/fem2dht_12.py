"""


"""
import meshio
import matplotlib.pylab as plt
import matplotlib.collections
import numpy as np
from scipy.sparse.linalg import spsolve
from scipy import sparse, linalg

def triquadplot(x, y, quatrangles, ax=None, **kwargs):
    if not ax: ax=plt.gca()
    xy = np.c_[x,y]
    verts=xy[quatrangles]
    pc = matplotlib.collections.PolyCollection(verts, **kwargs)
    ax.add_collection(pc)
    ax.autoscale()

class fem2dht():
    def __init__(self, elem_type: str):
        self.nnodes = 0
        self.nelements = 0
        self.elem_type = elem_type

    def create_nodes(self, coords: np.array):
        self.nodes = coords
        self.nnodes = len(self.nodes)

    def create_elements(self, connectivities: np.array):
        self.elements = []
        self.connectivities = connectivities

        if self.elem_type == 'tri':
            for nodes_element in connectivities:
                element = HT3(nodes_element)
                self.elements.append(element)
            self.nelements = len(self.elements)

        elif self.elem_type == 'quad':
            for nodes_element in connectivities:
                element = HT4(nodes_element)
                self.elements.append(element)
            self.nelements = len(self.elements)


    def define_props(self, E: float, v: float):
        D = (E/(1-v**2))*np.array([[      1, v,             0],
                                   [v,       1,             0],
                                   [      0,       0, (1-v)/2]])

        for i, element in enumerate(self.elements):
            element.props = D

    def create_bc(self, bc_type: str, nodes: list, values: list):

        if bc_type == 'T':
            self.bc_dirichlet_nodes = nodes
            self.bc_dirichlet_values = values


    def solve(self):

        rows = []
        cols = []
        values = []


        for element in self.elements:
            r, c, v = element.Kmatrix(self.nodes)
            rows.append(r)
            cols.append(c)
            values.append(v)


        rows = np.array(rows, dtype='int').flatten()
        cols = np.array(cols, dtype='int').flatten()
        values = np.array(values, dtype='float').flatten()

        Kglobal = sparse.csr_matrix((values, (rows, cols)), shape=((2*self.nnodes, 2*self.nnodes)))
        Kglobal = Kglobal + Kglobal.T - sparse.diags(Kglobal.diagonal(), dtype='float')

        self.Kglobal = Kglobal
        # 1) Montagem do vetor fglobal
        fglobal = np.zeros(2*self.nnodes)


        # 3) Aplicar as condições de contorno na matriz Kglobal
        w = 1e20
        nbcs = len(self.bc_dirichlet_nodes)
        for k in range(nbcs):
            node = self.bc_dirichlet_nodes[k]
            fglobal[node] = self.bc_dirichlet_values[k, 0] * w
            Kglobal[node, node] += w
            fglobal[node + 1] = self.bc_dirichlet_values[k, 1] * w
            Kglobal[node + 1, node + 1 ] += w

        self.Kglobal = Kglobal
        self.fglobal = fglobal

        # 5) Resolver o problema
        self.T = spsolve(Kglobal, fglobal)

        C_new = np.zeros((self.nnodes, 2))
        for k in range(self.nnodes):
            C_new[k, 0] = self.nodes[k, 0] + self.T[k]
            C_new[k, 1] = self.nodes[k, 1] + self.T[k+1]

        self.displ = C_new

    def plot(self):
        if self.elem_type == 'tri':

            plt.figure()
            triquadplot(self.displ[:, 0], self.displ[:, 1], self.connectivities, ax=None, facecolor='lightblue', edgecolor='midnightblue')
            plt.legend('Temperatura')
            plt.show()

        elif self.elem_type == 'quad':
            plt.figure()
            ax = plt.gca()
            xy = np.c_[self.nodes[:, 0], self.nodes[:, 1]]
            verts = xy[self.connectivities]
            pc = matplotlib.collections.PolyCollection(verts)
            pc.set_array(self.T)
            ax.add_collection(pc)
            ax.autoscale()
            plt.show()



class HT3:
    def __init__(self, nodes):
        self.nodes = nodes
        self.props = 0

    def Kmatrix(self, coords):
        x = coords[self.nodes, 0]
        y = coords[self.nodes, 1]
        x_ = np.array(coords[self.nodes])

        self.centroid = [np.mean(x), np.mean(y)]

        dpdx = [y[1] - y[2], y[2] - y[0], y[0] - y[1]]
        dpdy = [x[2] - x[1], x[0] - x[2], x[1] - x[0]]

        B = np.asarray([[dpdx[0],       0, dpdx[1],       0, dpdx[2],       0],
                        [      0, dpdy[0],       0, dpdy[1],       0, dpdy[2]],
                        [dpdy[0], dpdx[0], dpdy[1], dpdx[1], dpdy[2], dpdy[2]]])

        self.A = 0.5 * (x[0] * y[1] + x[2] * y[0] + x[1] * y[2] - x[2] * y[1] - x[0] * y[2] - x[1] * y[0])
        B = (1 / (2 * self.A)) * B
        self.B = B

        aux = np.matmul(B.transpose(), self.props)
        k = np.matmul(aux, B) * self.A

        nodes = np.array([self.nodes[0], self.nodes[0] + 1, self.nodes[1], self.nodes[1] + 1, self.nodes[2], self.nodes[2] + 1])

        index_ind_rows = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5]
        index_ind_cols = [0, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 2, 3, 4, 5, 3, 4, 5, 4, 5, 5]

        ind_rows = nodes[index_ind_rows]
        ind_cols = nodes[index_ind_cols]

        values = []
        for i, j in zip(index_ind_rows, index_ind_cols):
            values.append(k[i, j])

        return ind_rows, ind_cols, values


class HT4:

    def __init__(self, nodes: np.array):
        self.nodes = nodes
        self.props = 0.0

    def Kmatrix(self, coords: np.array):

        x_ = np.array(coords[self.nodes])
        self.centroid = [np.mean(x_[:, 0]), np.mean(x_[:, 1])]

        dp1dxi = lambda qsi, eta:  (1 / 4) * (eta - 1)
        dp2dxi = lambda qsi, eta:  (1 / 4) * (1 - eta)
        dp3dxi = lambda qsi, eta:  (1 / 4) * (1 + eta)
        dp4dxi = lambda qsi, eta:  (1 / 4) * (-1 - eta)

        dp1deta = lambda qsi, eta:  (1 / 4) * (-1 + qsi)
        dp2deta = lambda qsi, eta:  (1 / 4) * (-1 - qsi)
        dp3deta = lambda qsi, eta:  (1 / 4) * (1 + qsi)
        dp4deta = lambda qsi, eta:  (1 / 4) * (1 - qsi)

        GN = lambda qsi, eta: \
            np.array(  [[dp1dxi(qsi, eta),  dp2dxi(qsi, eta),  dp3dxi(qsi, eta),  dp4dxi(qsi, eta)],
                        [dp1deta(qsi, eta), dp2deta(qsi, eta), dp3deta(qsi, eta), dp4deta(qsi, eta)]])


        Jac = lambda qsi, eta, x_: \
            np.matmul(GN(qsi, eta), x_)

        def_B = lambda GN_xy : \
            np.array([[GN_xy[0, 0],           0, GN_xy[0, 1],           0, GN_xy[0, 2],           0, GN_xy[0, 3],          0],
                      [          0, GN_xy[1, 0],           0, GN_xy[1, 1],           0, GN_xy[1, 2],           0, GN_xy[1, 3]],
                      [GN_xy[1, 0], GN_xy[0, 0], GN_xy[1, 1], GN_xy[0, 1], GN_xy[1, 2], GN_xy[0, 2], GN_xy[1, 3], GN_xy[0, 3]]])

        qsi = [-1 / np.sqrt(3), 1 / np.sqrt(3)]
        eta = [-1 / np.sqrt(3), 1 / np.sqrt(3)]
        A = 4

        # integração gauss
        K = np.zeros((8, 8))
        for QSI in qsi:
            for ETA in eta:
                jacobiana = Jac(QSI, ETA, x_)
                GN_xy = np.matmul(np.linalg.inv(jacobiana), GN(QSI, ETA))
                B = def_B(GN_xy)
                aux = np.matmul( np.transpose(B), self.props)
                K_ = np.matmul(aux, B) * np.linalg.det(jacobiana)
                K = K + K_

        nodes = [self.nodes[0], self.nodes[0] + 1, self.nodes[1], self.nodes[0] + 1,
                 self.nodes[2], self.nodes[2] + 1, self.nodes[3], self.nodes[3] + 1]

        index_ind_rows = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 6, 6, 7]
        index_ind_cols = [0, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 2, 3, 4, 5, 6, 7, 3, 4, 5, 6, 7, 4, 5, 6, 7, 5, 6, 7, 6, 7, 7]

        #ind_rows = self.nodes[index_ind_rows]
        #ind_cols = self.nodes[index_ind_cols]

        ind_rows = nodes[index_ind_rows]
        ind_cols = nodes[index_ind_cols]

        values = []
        for i, j in zip(index_ind_rows, index_ind_cols):
            values = K[i, j]

        return ind_rows, ind_cols, values


if __name__ == '__main__':

    problem = fem2dht()

    mesh = meshio.read('ex1_mesh1_tri.msh')
    coords = np.array(mesh.points[:, 0:2])
    connectivities = mesh.cells['triangle']

    # Implementação do elemento Q4
    # connectivities = ([0, 1, 2, 3])

    # (1) - Geometry and mesh
    problem.create_nodes(coords)
    problem.create_elements(connectivities)

    # TODO: alterar para os nós da malha importada
    # Buscar as posições dos pontos de interesse
    # Nó 0


    nodes = [0, 2, 4]
    values = [10, 5, 7.5]

    # (2) - Properties
    problem.define_props(2.1e9, 0.3)

    # (3) - Boundary conditions
    problem.create_bc('T', nodes, values)

    # (4) - Solve
    problem.solve()

    # (5) - Postprocessing
    problem.plot()



