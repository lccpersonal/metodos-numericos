from mpl_toolkits import mplot3d
import numpy as np
from matplotlib import cm
import matplotlib as mpl
import matplotlib.pyplot as plt

N1 = lambda eta, xi: -(1 - eta) * (1 - xi) * (1 + xi + eta) / 4
N2 = lambda eta, xi: -(1 - eta) * (1 + xi) * (1 - xi + eta) / 4
N3 = lambda eta, xi: -(1 + eta) * (1 + xi) * (1 - xi - eta) / 4
N4 = lambda eta, xi: -(1 + eta) * (1 - xi) * (1 + xi - eta) / 4

N5 = lambda eta, xi: (1 - eta) * (1 - xi) * (1 + xi) / 2
N6 = lambda eta, xi: (1 - eta) * (1 + xi) * (1 + eta) / 2
N7 = lambda eta, xi: (1 + eta) * (1 - xi) * (1 + xi) / 2
N8 = lambda eta, xi: (1 - eta) * (1 - xi) * (1 + eta) / 2


if __name__ == '__main__':

   xi = np.linspace(-1, 1, num=20)
    eta = np.linspace(-1, 1, num=20)

    data = np.zeros((len(xi), len(eta)))
    for j, _eta in enumerate(eta):
        for i, _xi in enumerate(xi):
            data[i, j] = N1(_xi, _eta)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    XI, ETA = np.meshgrid(xi, eta)
    surf = ax.plot_surface(XI, ETA, data, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.title('Gráfico 3D do caso ')
    plt.show()
