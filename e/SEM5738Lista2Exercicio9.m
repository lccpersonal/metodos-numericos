%% SEM5738 - M�todos Num�ricos
%{
Departamento de Engenharia Mec�nica
Escola de Engenharia de S�o Carlos - Universidade de S�o Paulo
�rea de Concentra��o: Din�mica e Mecatr�nica
Eng. Mecatr�nico �caro Ostan    n� USP: 8549897
%}

clc
clear all
close all
%%
% Discretization of time
nt = 30;
t = linspace(0, 30, nt + 1);
dt = t(2)-t(1);

% Discratization of space
nx = 100;
x = linspace(0, 1, nx + 1);
dx = x(2) - x(1);

% Problem parameters
rho = 1;    %kg/m^3
c = 100;    %J/kg/K
k = 1;      %W/m^2/K
L = 1/(length(x)-1);      %m

% Initial conditions
% q_1(t) = 1, for t <= 10;
% q_1(t) = 0, for t > 10;
% q_2(t) = 0, for all t;
% T(x ,t = 0) = 1

for k = 1 : length(t)
    if t(k) > 10
        q1(k) = [0];
    else
        q1(k) = [1];
    end
end

q = zeros(length(x),1,length(t));

for k = 1 : length(t)
    q(1,:,k) = q1(k);
end

T = zeros(length(x),1,length(t));

T(:,:,1) = 1;

% Boundary conditions
% q(x = L, t) = 0

% Inertia matrix
C = rho*L*c*[1/3, 1/6; 1/6, 1/3];

%Stiffness matrix
K = [1 -1; -1 1]*1/L;

% f matrix
f = q;

%
iG_K = zeros(4, nx);
jG_K = zeros(4, nx);
vG_K = zeros(4, nx);

%
iG_C = zeros(4, nx);
jG_C = zeros(4, nx);
vG_C = zeros(4, nx);

parfor n = 1 : nx
    i = [n; n+1; n; n+1];
    j = [n; n; n+1; n+1];
    
    v_K = reshape(K, 1, 4);
    iG_K(:, n) = i;
    jG_K(:, n) = j;
    vG_K(:, n) = v_K;
    
    v_C = reshape(C, 1, 4);
    iG_C(:, n) = i;
    jG_C(:, n) = j;
    vG_C(:, n) = v_C;
end

K = sparse(reshape(iG_K, 1, 4*nx), reshape(jG_K, 1, 4*nx), reshape(vG_K, 1, 4*nx));
C = sparse(reshape(iG_C, 1, 4*nx), reshape(jG_C, 1, 4*nx), reshape(vG_C, 1, 4*nx));

for k = 1 : nt - 1
    T(:, :, k + 1) = ( C + dt*K )\( C*T(:, :, k) + dt*f(:, :, k + 1) );
end

figure
for k  = 1 : length(t)-1
    if k < 11
        plot(x,T(:,1,k), 'r')
    else
        plot(x,T(:,1,k),'b')
    end
    hold on
end
grid on
title('Temperature of the finite element over 30 seconds')
set(gca, 'FontName', 'CMU Concrete')
xlabel('Length (m)')
ylabel('Temperature')