%Henrique Takashi Idogava. N� USP: 10919257. 
% Lista 2 : Quest�o 11 (Equil�brio de S�lidos (treli�a)

clear all
close all

nel=15;          % n�mero de elementos (segmentos)
nnel=2;          % n�mero de n�s por elemento
ndof=2;          % numero de graus de liberdade por n�
nnode=9;         % numero total de n�s
sdof=nnode*ndof; % numero total de graus de liberdade

% Coordenadas dos n�s no sistema X-Y

gcoord(1,1)=0.0;    gcoord(1,2)=0.0;   
gcoord(2,1)=0.3;    gcoord(2,2)=0.0;   
gcoord(3,1)=0.6;    gcoord(3,2)=0.0;  
gcoord(4,1)=0.9;    gcoord(4,2)=0.0;   
gcoord(5,1)=1.2;    gcoord(5,2)=0.0;   
gcoord(6,1)=0.15;   gcoord(6,2)=0.25981;   
gcoord(7,1)=0.45;   gcoord(7,2)=0.25981;   
gcoord(8,1)=0.75;   gcoord(8,2)=0.25981;  
gcoord(9,1)=1.05;   gcoord(9,2)=0.25981;   

%Propriedades

E = 210e9;
A = 0.000003142;

elprop = zeros(nel,2);
elprop(:,1) = E;
elprop(:,2) = A;

%Conectividade entre os n�s dos elementos

nodes(1,1)=1;  nodes(1,2)=2;   
nodes(2,1)=2;  nodes(2,2)=3;  
nodes(3,1)=3;  nodes(3,2)=4;   
nodes(4,1)=4;  nodes(4,2)=5;   
nodes(5,1)=1;  nodes(5,2)=6;  
nodes(6,1)=2;  nodes(6,2)=6;   
nodes(7,1)=2;  nodes(7,2)=7;   
nodes(8,1)=3;  nodes(8,2)=7;   
nodes(9,1)=3;  nodes(9,2)=8;   
nodes(10,1)=4;  nodes(10,2)=8;   
nodes(11,1)=4;  nodes(11,2)=9;   
nodes(12,1)=5;  nodes(12,2)=9;   
nodes(13,1)=6;  nodes(13,2)=7;   
nodes(14,1)=7;  nodes(14,2)=8;   
nodes(15,1)=8;  nodes(15,2)=9;   

%Condi��es de Contorno de acordo com os graus de liberdade

bcdoff(1)=1;    %Restri��o de movimento no grau de liberdade 1 (N� 1, dire��o x)
bcval(1)=0;     %Valor zero para o grau de liberdade 1
bcdoff(2)=2;    %Restri��o de movimento no grau de liberdade 2 (N� 1, dire��o y)
bcval(2)=0;     %Valor zero para o grau de liberdade 2
bcdoff(3)=10;   %Restri��o de movimento no grau de liberdade 10 (N� 5, dire��o y)
bcval(3)=0;     %Valor zero para o grau de liberdade 10


ff=zeros(sdof,1);              % system force vector
kk=zeros(sdof,sdof);           % system stiffness matrix
index=zeros(nnel*ndof,1);      % index vector
elforce=zeros(nnel*ndof,1);    % element force vector
eldisp=zeros(nnel*ndof,1);     % element nodal displacement vector
k=zeros(nnel*ndof,nnel*ndof);  % element stiffness matrix
stress=zeros(nel,1);           % stress vector for every element

%Aplica��o das For�as

ff(14)=-4000;     % For�a de 5000N no 7� N�, dire��o y.
ff(16)=-2000;     % For�a de 2000N no 8� N�, dire��o y.

%Calculo dos Deslocamentos Nodais

for iel=1:nel    

nd(1)=nodes(iel,1);   
nd(2)=nodes(iel,2);   
x1=gcoord(nd(1),1); y1=gcoord(nd(1),2);  
x2=gcoord(nd(2),1); y2=gcoord(nd(2),2);  

leng=sqrt((x2-x1)^2+(y2-y1)^2);  

if (x2-x1)==0; 
beta=2*atan(1);      
else
beta=atan((y2-y1)/(x2-x1));
end

el=elprop(iel,1);               
area=elprop(iel,2);            

index=feeldof(nd,nnel,ndof);  

k=fetruss2(el,leng,area,0,beta,1);

kk=feasmbl1(kk,k,index);           

end

n=length(bcdoff);
sdof=size(kk);

 for i=1:n
    c=bcdoff(i);
    for j=1:sdof
       kk(c,j)=0;
    end

    kk(c,c)=1;
    ff(c)=bcval(i);
 end

%[kk,ff]=feaplyc2(kk,ff,bcdoff,bcval);  

disp=kk\ff;   

%Calculo das Tens�es

for iel=1:nel        

nd(1)=nodes(iel,1);   
nd(2)=nodes(iel,2);   

x1=gcoord(nd(1),1); y1=gcoord(nd(1),2);  
x2=gcoord(nd(2),1); y2=gcoord(nd(2),2);  

leng=sqrt((x2-x1)^2+(y2-y1)^2);  

if (x2-x1)==0; 
    beta=2*atan(1);       
else
    beta=atan((y2-y1)/(x2-x1));
end

el=elprop(iel,1);               
area=elprop(iel,2);            
index=feeldof(nd,nnel,ndof);  

[k,m]=fetruss2(el,leng,area,0,beta,1); 

for i=1:(nnel*ndof)           
eldisp(i)=disp(index(i));     
end

elforce=k*eldisp;             
stress(iel)=sqrt(elforce(1)^2+elforce(2)^2)/area; 

if ((x2-x1)*elforce(3)) < 0;
stress(iel)=-stress(iel);
end

end

num=1:1:sdof;
format long
displ=[num' disp]          % Deslocamentos

numm=1:1:nel;
stresses=[numm' stress]    % Tens�es

for ie=1:nel
    n(1)=nodes(ie,1);
    n(2)=nodes(ie,2);

    index=[n(1)*2-1 n(1)*2 n(2)*2-1 n(2)*2];
    for i=1:(nnel*ndof)
        edisp(i)=disp(index(i));
        edisp1(i)=displ(index(i));
    end
    x0(1)=gcoord(nodes(ie,1),1); 
    y0(1)=gcoord(nodes(ie,1),2);
    x0(2)=gcoord(nodes(ie,2),1);
    y0(2)=gcoord(nodes(ie,2),2);
    x(1)=x0(1)+edisp(1); 
    y(1)=y0(1)+edisp(2);
    x(2)=x0(2)+edisp(3); 
    y(2)=y0(2)+edisp(4);

    axis([-0.1 1.3 -0.04 0.3])
    plot(x0,y0,'b',x,y,'r')
    
    legend('N�o-deformado','Deformado')
    grid
    hold on
end

