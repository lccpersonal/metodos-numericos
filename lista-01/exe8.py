import numpy as np
import math

def derivative(order, npoints, type):
    """
    Based on Randall Leveque
    :param order: Identify the order; Must be either 1 or 2
    :param npoints: Number of points
    :param type: Must be either B, C or F
    :return: returns a vector with coefficies
    """

    if npoints < order + 1:
        raise Exception(' The number of points must be at least the derivative order plus one')

    if type not in ['C', 'B', 'F']:
        raise Exception(' Not a valid type of finite difference. Must be either C (Central), F (Forward) or B (Backward)')

    if type in ['B', 'F']:
        x = np.asarray(list(range(npoints + 1)))
    elif npoints % 2 == 0:
        raise Exception('Please, enter with a odd number for central approximation')
    else:
        n_ = int((npoints - 1)/2)
        x = np.asarray(list(range(-1, -(n_ + 1), -1))[::-1] + list(range(n_ + 1)))

    npoints_ = len(x)
    a = np.ones((npoints_, npoints_))
    b = np.zeros(npoints_)
    b[order] = 1

    for k in range(1, npoints_):
        a[k] = np.power(x, k) / math.factorial(k)

    coefs = np.linalg.lstsq(a, b, rcond=None)

    coefs = np.asarray(coefs)

    if type == 'B':
        coefs = -1*coefs

    return coefs[0]


if __name__ == '__main__':
    order = 2
    npoints = 5
    type_ = 'B'
    out = derivative(order, npoints, type_)
    print(out)




