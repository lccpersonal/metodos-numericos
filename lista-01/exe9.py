from mpl_toolkits import mplot3d
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm


def explicito(T, X, q, lambda_):
    dt = T[1] - T[0]
    dx = X[1] - X[0]

    if 2*lambda_*dt/(dx*dx) >= (1):
        raise Exception(" Algoritmo Não Converge!", 2*lambda_*dt/(dx*dx), 'deve ser menor ou igual a 1')

    coeff = lambda_ * dt / (dx * dx)

    u = np.zeros((len(X), len(T)))
    u[:, 0] = np.ones(len(X))

    for t in range(len(T) - 1):  # time

        for x in range(1, len(X) - 1):  # space
            u[x, t + 1] = coeff * (u[x + 1, t] - 2 * u[x, t] + u[x - 1, t]) + u[x, t]

        u[0, t + 1] = u[0, t] + coeff * (u[1, t] - 2 * u[0, t] + (-dx * q[t] + u[0, t]))
        u[-1, t+1] = u[-1, t] + coeff*(u[-2, t] - 2*u[-1, t] + (u[-1, t]))
    return u


def implicito(T, X, q, lambda_):
    dt = T[1] - T[0]
    dx = X[1] - X[0]

    coeff = (lambda_ * dt) / (dx * dx)

    u = np.ones((len(X), len(T)))

    MM = np.diag((1 + 2 * coeff) * np.ones(len(X))) \
         + np.diag((-coeff) * np.ones(len(X) - 1), 1) \
         + np.diag((-coeff) * np.ones(len(X) - 1), -1)

    #Boundary Conditions
    MM[0, 0:3] = np.asarray([3, -4, 1]) * (1 / (2 * dx))
    MM[-1, -3:] = np.asarray([-1, 4, -3]) * (-1 / (2 * dx))
    #Inverse
    MM = np.linalg.inv(MM)

    for t in range(len(T) - 1):  # Time Domain

        k1 = u[0, t]
        k2 = u[-1, t]

        u[0, t] = -1*q[t]
        u[-1, t] = 0
        u[:, t + 1] = np.matmul(MM, u[:, t])

        u[0, t] = k1
        u[-1, t] = k2

    return u


def plot_graf(t, x, data, caso):

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    T, X = np.meshgrid(t, x)
    surf = ax.plot_surface(X, T, data, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.title('Gráfico 3D do caso ' + caso)
    plt.show()

    plt.figure()
    for k in range(len(t)):
        if k % 10 == 0:
            if t[k] > 10:
                plt.plot(x, data[:, k], 'b')
            else:
                plt.plot(x, data[:, k], 'r')
    plt.title('Gráficos por tempo do caso ' + caso)

    with mpl.rc_context(rc={'interactive': False}):
        plt.show()

    return 0


if __name__ == '__main__':
    lambda_ = 0.01
    nt = 300
    nx = 20

    t = np.linspace(0, 30, num=nt + 1)
    x = np.linspace(0, 1, num=nx + 1)

    q = np.array([0 if t_ > 10 else (-1) for t_ in t])


    plt.figure()
    plt.plot(t, q)
    plt.title("Condição de Fluxo conforme o tempo")
    plt.xlabel("t")
    plt.ylabel("q")
    plt.show()

    out1 = explicito(t, x, q, lambda_)
    plot_graf(t, x, out1, 'explícito')
    out2 = implicito(t, x, q, lambda_)
    plot_graf(t, x, out2, 'implícito')
