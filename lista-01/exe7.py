'''
Ex 7 - Lista 01 Métodos Numéricos


'''

import math
import numpy as np
import matplotlib.pyplot as plt

xx = np.linspace(0, math.pi, num=100)
ddx = np.linspace(0.001, 1, num=1000)

func = lambda x: math.cos(x) + math.sin(x)
D1 = lambda x: - math.sin(x) + math.cos(x)

# Central
C3 = np.zeros(len(ddx))
C5 = np.zeros(len(ddx))

# Backward
B2 = np.zeros(len(ddx))
B3 = np.zeros(len(ddx))
B4 = np.zeros(len(ddx))

# Forward
F2 = np.zeros(len(ddx))
F3 = np.zeros(len(ddx))
F4 = np.zeros(len(ddx))

for k, dx in enumerate(ddx):

    for x in xx:
        value = np.array([func(x - 3 * dx), func(x - 2 * dx), func(x - dx), func(x), func(x + dx), func(x + 2 * dx),
                          func(x + 3 * dx)])
        exact_solution = D1(x)

        # C3
        dy = ((-0.5) * value[2] + (0.5) * value[4]) / (dx)
        C3[k] = C3[k] + (exact_solution - dy) * (exact_solution - dy)

        # C5
        dy = ((1 / 12) * value[1] + (-2 / 3) * value[2] + (2 / 3) * value[4] + (-1 / 12) * value[5]) / (dx)
        C5[k] = C5[k] + (exact_solution - dy) * (exact_solution - dy)

        # B2
        dy = (value[3] + (-1)*value[2]) / (dx)
        B2[k] = B2[k] + (exact_solution - dy) * (exact_solution - dy)

        # B3
        dy = ((3 / 2) * value[3] + (-2) * value[2] + (1 / 2) * value[1]) / (dx)
        B3[k] = B3[k] + (exact_solution - dy) * (exact_solution - dy)

        # B4
        dy = ((11/ 6) * value[3] + (-3) * value[2] + (3/2) * value[1] + (-1 / 3) * value[0]) / (dx)
        B4[k] = B4[k] + (exact_solution - dy) * (exact_solution - dy)

        # F2
        dy = ((-1) * value[3] + value[4]) / (dx)
        F2[k] = F2[k] + (exact_solution - dy) * (exact_solution - dy)

        # F3
        dy = ((-3 / 2) * value[3] + (2) * value[4] + (-1 / 2) * value[5]) / (dx)
        F3[k] = F3[k] + (exact_solution - dy) * (exact_solution - dy)

        # F4
        dy = ((-11 / 6) * value[3] + (3) * value[4] + (-3 / 2) * value[5] + (1 / 3) * value[6]) / (dx)
        F4[k] = F4[k] + (exact_solution - dy) * (exact_solution - dy)

C3 = (1 / len(xx)) * np.sqrt(C3)
C5 = (1 / len(xx)) * np.sqrt(C5)
B2 = (1 / len(xx)) * np.sqrt(B2)
B3 = (1 / len(xx)) * np.sqrt(B3)
B4 = (1 / len(xx)) * np.sqrt(B4)
F2 = (1 / len(xx)) * np.sqrt(F2)
F3 = (1 / len(xx)) * np.sqrt(F3)
F4 = (1 / len(xx)) * np.sqrt(F4)

plt.figure()
plt.plot(ddx, C3)
plt.plot(ddx, C5)
plt.plot(ddx, B2, linestyle='dashed')
plt.plot(ddx, B3, linestyle='dashed')
plt.plot(ddx, B4, linestyle='dashed')
plt.plot(ddx, F2, marker=',')
plt.plot(ddx, F3, marker=',')
plt.plot(ddx, F4, marker=',')
plt.xlabel('dx')
plt.legend(['C3', 'C5', 'B2', 'B3', 'B4', 'F2', 'F3', 'F4'])
plt.show()
