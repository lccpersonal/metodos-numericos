'''
Ex 5 - Lista 01 Métodos Numéricos


'''

import math
import numpy as np

def analise_seno( x, dx):
    """
    Análise Máxima até com 5 pontos
    """

    solucao_exata = math.sin(x)
    print('solucao_exata:', solucao_exata)

    vetor_cos = np.array([math.cos(x - 2*dx), math.cos(x - dx), math.cos(x), math.cos(x + dx), math.cos(x + 2*dx)])
    #zero_index = math.floor(len(vetor_cos)) + 1
    print("vetor_cos:", vetor_cos)

    #Backward 2
    c = coefficients('B2')
    dy = (vetor_cos[2] - vetor_cos[1]) / (dx)
    print('dy B2:', dy)

    #Backward 3
    dy = (+3*vetor_cos[2] - 4*vetor_cos[1] + vetor_cos[0]) / (2*dx)
    print('dy B3:', dy)

    #Forward 2
    dy = (-vetor_cos[2] + vetor_cos[3]) / (dx)
    print('dy F2:', dy)

    #Forward 3
    dy = (-3*vetor_cos[2] + 4*vetor_cos[3] -vetor_cos[4]) / (2*dx)
    print('dy F3:', dy)

    #Central 2
    dy = (-vetor_cos[1] + vetor_cos[3]) / (2*dx)
    print('dy C3:', dy)

    #Central 4
    dy = (vetor_cos[0] - 8*vetor_cos[1] + 8*vetor_cos[3] - vetor_cos[4]) / (12*dx)
    print('dy C4:', dy)

    return 0

def coefficients(type_fd):
    if type_fd == 'B2':
        return [1, -1]
    elif type_fd == 'B3':
        return [3/2, -2, -1/2]
    elif type_fd == 'F2':
        return [1, -1]
    elif type_fd == 'F3':
        return [-1/2, 2, -1/2]
    elif type_fd == 'C3':
        return [-1/2, 1/2]
    elif type_fd == 'C4':
        return [1/12, -8/12, 8/12, -1]
    else:
        return 0


if __name__ == '__main__':

    x = 0.3
    dx = 0.1

    analise_seno(x, dx)