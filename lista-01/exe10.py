from mpl_toolkits import mplot3d
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm

def explicito(T, X, Y, cond_inicial, lambda_):

    dx = X[1] - X[0]
    dy = Y[1] - Y[0]
    dt = T[1] - T[0]

    if (2*lambda_*dt)/(min(dx*dx, dy*dy)) >= 1:
        raise Exception(" Algoritmo Não Converge!:", (2*lambda_*dt)/(min(dx*dx, dy*dy)), 'deve ser menor ou igual a 1')

    #Condição Inicial
    u = np.zeros((len(Y), len(X), len(T)))

    for x in range(len(X)):
        for y in range(len(Y)):
            u[y, x, 0] = cond_inicial(X[x], Y[y])

    #Initial Condition
    plt.matshow(u[:, :, 0])
    plt.show()

    cx = (lambda_ * dt) / (dx * dx)
    cy = (lambda_ * dt) / (dy * dy)

    #Explicit Resolution
    for t in range(len(T) - 1):

        for x in range(len(X) - 1):
            for y in range(len(Y) - 1):
                u[y, x, t+1] = u[y, x, t] + \
                        cx * (u[y, x + 1, t] - 2*u[y, x, t] + u[y, x - 1, t]) + \
                        cy * (u[y + 1, x, t] - 2*u[y, x, t] + u[y - 1, x, t])

                # Bondary Conditions Borders
                u[0, x, t+1] = u[0, x, t] + \
                               cx*(u[0, x + 1, t] - 2*u[0, x, t] + u[0, x - 1, t]) + \
                               cy*(u[1, x, t] - 2*u[0, x, t] + u[1, x, t])

                u[y, 0, t+1] = u[y, 0, t] + \
                                 cx * (u[y, 1, t] - 2 * u[y, 0, t] + u[y, 1, t]) + \
                                 cy * (u[y+1, 0, t] - 2*u[y, 0, t] + u[y-1, 0, t])

                u[-1, x, t + 1] = u[-1, x, t] + \
                                 cx * (u[-1, x + 1, t] - 2 * u[-1, x, t] + u[-1, x - 1, t]) + \
                                 cy * (u[-2, x, t] - 2 * u[-1, x, t] + u[-2, x, t])

                u[y, -1, t + 1] = u[y, -1, t] + \
                                 cx * (u[y, -2, t] - 2 * u[y, -1, t] + u[y, -2, t]) + \
                                 cy * (u[y + 1, -1, t] - 2 * u[y, -1, t] + u[y - 1, -1, t])

        #Boundary Conditions Vertices
        u[0, 0, t + 1] = u[0, 0, t] + \
                cx * (u[0, 1, t] - 2 * u[0, 0, t] + u[0, 1, t]) + \
                cy * (u[1, 0, t] - 2 * u[0, 0, t] + u[1, 0, t])

        u[-1, -1, t + 1] = u[-1, -1, t] + \
                         cx * (u[-1, -2, t] - 2 * u[-1, -1, t] + u[-1, -2, t]) + \
                         cy * (u[-2, -1, t] - 2 * u[-1, -1, t] + u[-2, -1, t])

        u[0, -1, t + 1] = u[0, -1, t] + \
                           cx * (u[0, -2, t] - 2 * u[0, -1, t] + u[0, -2, t]) + \
                           cy * (u[1, -1, t] - 2 * u[0, -1, t] + u[1, -1, t])

        u[-1, 0, t + 1] = u[-1, 0, t] + \
                         cx * (u[-1, 1, t] - 2 * u[-1, 0, t] + u[-1, 1, t]) + \
                         cy * (u[-2, 0, t] - 2 * u[-1, 0, t] + u[-2, 0, t])

        debug(u[:, :, t+1], T[t])

    plt.contour(u[:, :, -1])
    plt.title('Isotermas no Tempo= 100')
    plt.show()


    #plt.matshow(u[:, :, -1])
    #plt.title("Resolução Método Explícito")
    #plt.show()

    return u

def debug(data, tempo):
    if tempo%25==0:
        #plt.matshow(data)
        plt.contour(data)
        plt.title('Isotermas no Tempo='+ str(tempo))
        plt.show()

def implicito(T, X, Y, cond_inicial, lambda_):

    lx = len(X)
    ly = len(Y)
    xend = len(X)-1
    yend = len(Y)-1

    # Initial Condition
    u = np.zeros((lx, ly, len(T)))
    # New Mapping for u
    u_ = np.zeros((lx*ly, len(T)))

    for x in range(lx):
        for y in range(ly):
            u[y, x, 0] = cond_inicial(X[x], Y[y])
            u_[m(x, y), 0] = u[y, x, 0]

    cx = (lambda_ * dt) / (dx * dx)
    cy = (lambda_ * dt) / (dy * dy)

    MM = np.diag((1 + 2 * cx + 2*cy) * np.ones(lx*ly)) \
         + np.diag((-cx) * np.ones(lx*ly - 1), 1) \
         + np.diag((-cx) * np.ones(lx*ly - 1), -1) \
         + np.diag((-cy) * np.ones(lx*ly - lx), lx) \
         + np.diag((-cy) * np.ones(lx*ly - lx), -lx)

    # Boundary Condition for y=0 and y=end
    for p_ in range(1, lx-1):
        MM = border_boundary_conditions(MM, (p_, 0), (xend, yend), (dx, dy))
        MM = border_boundary_conditions(MM, (p_, yend), (xend, yend), (dx, dy))

    # Boundary Condition for y=0 and y=end
    for p_ in range(1, ly - 1):
        MM = border_boundary_conditions(MM, (0, p_), (xend, yend), (dx, dy))
        MM = border_boundary_conditions(MM, (xend, p_), (xend, yend), (dx, dy))

    MM = vertices_variable_conditions(MM, (0, 0), (xend, yend), (dx, dy))
    MM = vertices_variable_conditions(MM, (xend, 0), (xend, yend), (dx, dy))
    MM = vertices_variable_conditions(MM, (0, yend), (xend, yend), (dx, dy))
    MM = vertices_variable_conditions(MM, (xend, yend), (xend, yend), (dx, dy))

    #Inverse
    MM = np.linalg.inv(MM)

    for t in range(len(T) - 1):

        #Save Boundaries
        v1 = u_[m(0, 0), t]
        v2 = u_[m(0, yend), t]
        v3 = u_[m(xend, 0), t]
        v4 = u_[m(xend, yend), t]

        b1 = u_[[m(0, p) for p in range(1, ly - 1)], t]
        b2 = u_[[m(p, 0) for p in range(1, lx - 1)], t]
        b3 = u_[[m(xend, p) for p in range(1, ly - 1)], t]
        b4 = u_[[m(xend, yend) for p in range(1, lx - 1)], t]

        #Update Boundaries
        u_[m(0, 0), t] = 0
        u_[m(0, yend), t] = 0
        u_[m(xend, 0), t] = 0
        u_[m(xend, yend), t] = 0

        u_[[m(0, p) for p in range(1, ly - 1)], t] = 0
        u_[[m(p, 0) for p in range(1, lx - 1)], t] = 0
        u_[[m(xend, p) for p in range(1, ly - 1)], t] = 0
        u_[[m(p, yend) for p in range(1, lx - 1)], t] = 0

        u_[:, t + 1] = np.matmul(MM, u_[:, t])

        # Update Boundaries
        u_[m(0, 0), t] = v1
        u_[m(0, yend), t] = v2
        u_[m(xend, 0), t] = v3
        u_[m(xend, yend), t] = v4

        u_[[m(0, p) for p in range(1, ly - 1)], t] = b1
        u_[[m(p, 0) for p in range(1, lx - 1)], t] = b2
        u_[[m(xend, p) for p in range(1, ly - 1)], t] = b3
        u_[[m(p, yend) for p in range(1, lx - 1)], t] = b4

        for x in range(lx):
            for y in range(ly):
                u[y, x, t+1] = u_[x * lx + y, t+1]



    plt.matshow(u[:, :, -2])
    plt.title("Resolução Método Implícito")
    plt.show()

    return u

def m(x, y, *args):
    try:
        global LEN_X
    except NameError:
        LEN_X = int(*args)

    index = x*LEN_X + y
    return index

def inv_m(index, *args):
    try:
        global LEN_X
    except NameError:
        LEN_X = int(*args)

    x = math.floor(index/LEN_X)
    y = index % LEN_X
    return y, x


def border_boundary_conditions(MM, point, limits, derivatives):

    x, y = point
    xend, yend = limits
    dx, dy = derivatives

    if x == 0:
        #Forward
        x_coeff = [-3/2, 2, -1/2]
        x_p =     [   0, 1,    2]
    elif x == xend:
        #Backward
        x_coeff = [ 3/2,       -2,       1/2]
        x_p =     [ xend, xend - 1, xend - 2]
    else:
        #Central
        x_coeff = [-1/2, 1/2]
        x_p =     [ x-1, x+1]

    if y == 0:
        #Forward
        y_coeff = [-3/2, 2, -1/2]
        y_p = [0, 1, 2]
    elif y == yend:
        #Backward
        y_coeff = [3/2, -2, 1/2]
        y_p = [yend, yend - 1, yend - 2]
    else:
        #Central
        y_coeff = [-1/2, 1/2]
        y_p = [y-1, y+1]

    for j in range(len(x_coeff)):
        MM[m(x, y), m(x_p[j], y)] = x_coeff[j]/dx

    for i in range(len(y_coeff)):
        MM[m(x, y), m(x, y_p[i])] = y_coeff[i]/dy

    return MM

def vertices_variable_conditions(MM, point, limits, derivatives):

    x, y = point
    xend, yend = limits
    dx, dy = derivatives

    if x == 0:
        #Forward
        x_coeff = [-3/2, 2, -1/2]
        x_p =     [   0, 1,    2]
    else:
        #Backward
        x_coeff = [ 3/2,       -2,       1/2]
        x_p =     [ xend, xend - 1, xend - 2]

    if y == 0:
        #Forward
        y_coeff = [-3/2, 2, -1/2]
        y_p =     [0   , 1   , 2]
    else:
        #Backward
        y_coeff = [3/2, -2, 1/2]
        y_p = [yend, yend - 1, yend - 2]

    for j in range(len(x_coeff)):
        for i in range(len(y_coeff)):
            MM[m(x, y), m(x_p[j], y_p[i])] = x_coeff[j] * y_coeff[i] / (dx*dy)

    return MM

if __name__ == '__main__':

    nt = 100
    nx = 50
    ny = 50

    lambda_ = 0.01
    t = np.linspace(0, 100, num=nt+1)
    x = np.linspace(0, 2, num=nx+1)
    y = np.linspace(0, 2, num=ny+1)

    globals()['LEN_X'] = nx + 1

    cond_inicial = lambda x, y: 10 * math.exp(-20 * ((x-1) * (x-1) + (y-1) * (y-1)))

    dt = t[1] - t[0]
    dx = x[1] - x[0]
    dy = y[1] - y[0]

    #out1 = explicito(t, x, y, cond_inicial, lambda_)
    out2 = implicito(t, x, y, cond_inicial, lambda_)

    plt.figure()
    plt.matshow(out2[:, :, 0])
    plt.show()

