'''
Ex 6 - Lista 01 Métodos Numéricos


'''

import math
import numpy as np
import matplotlib.pyplot as plt

x = 2
ddx = np.linspace(0.001, 2, num=1000)

func = lambda x: math.exp(x)*math.sin(x)
F1 = lambda x: math.exp(x)*(math.cos(x) + math.sin(x))
F2 = lambda x: 2*math.exp(x)*(math.cos(x))

solucao_f1 = F1(x)
solucao_f2 = F2(x)


e_f1c2 = np.zeros(len(ddx))
e_f1c4 = np.zeros(len(ddx))

e_f2c2 = np.zeros(len(ddx))
e_f2c4 = np.zeros(len(ddx))

for k in range(len(ddx)):

    dx = ddx[k]
    value = np.array([func(x - 2*dx),func(x - dx), func(x), func(x + dx), func(x + 2*dx)])

    #F1 Central 2
    dy = (-value[1] + value[3]) / (2*dx)
    e_f1c2[k] = solucao_f1 - dy

    #F1 C4 
    dy = (value[0] -8*value[1] + 8*value[3] - value[4]) / (12*dx)
    e_f1c4[k] = solucao_f1 - dy

    #F2 C3
    dy = ( value[1] + -2*value[2] + value[3]) / (dx*dx)
    #dy = ( -value[1] + value[3]) / (dx)
    e_f2c2[k] = solucao_f2 - dy

    #F2 C5
    #dy = (value[0] - 1*value[1] + 1*value[3] - value[4]) / (3*dx*dx)
    dy = (-value[0] + 16*value[1] -30*value[2] + 16*value[3] -value[4]) / (12*dx*dx)
    e_f2c4[k] = solucao_f2 - dy
    

plt.figure()
plt.plot(ddx, e_f1c2)
plt.plot(ddx, e_f1c4)
plt.plot(ddx, e_f2c2)
plt.plot(ddx, e_f2c4)
plt.xlabel('dx')
plt.ylabel('Erro')
plt.legend(['Derivada 1 de Central 2 pontos', 'Derivada 1 de Central 4 pontos', 'Derivada 2 de Central 2 pontos', 'Derivada 2 de Central 4  pontos'])
plt.grid(True)
plt.show()
